<?php

namespace App\Petitions\Repo;


use App\Accounts\Enums\AccountStatusEnum;
use App\Helper;
use App\Petitions\Enums\PetitionsStatusEnum;
use App\Restaurants\Model\Restaurants;

class FormatData
{
    public function formatAccount($accounts)
    {
        foreach ($accounts as $account) {
            $statusAccount = $account->status;
            $account->status = $this->statusAccount($statusAccount);
            $currency_ = Helper::json_object($account->currency);
            $currency = Helper::json_object($currency_);
            $account->currency = [
                'symbol' => $currency->symbol,
                'iso' => $currency->iso,
                'decimals' => $currency->decimals,
                'decimal_separator' => $currency->decimal_separator,
                'thousand_separator' => $currency->thousand_separator

            ];
            $account->details = count($account->details) > 0 ? Helper::object_array($this->details_invoice($account->details)) : [];
            $account->details_exist = count($account->details) > 0 ? true : false;
            if (count($account->petitions) > 0) {
                foreach ($account->petitions as $petition) {
                    $statusPetition = $petition->status;
                    $petition->status_ = $statusPetition;
                    $petition->status = $this->statusPetitions($statusPetition);
                    $petition->cancelled = $statusPetition == PetitionsStatusEnum::$cancelled ? true : false;
                }

                $sub_total = $this->subTotal($account->petitions, $currency->iso);
                $account->sub_total = $this->formatCurrency($currency_, $sub_total);
            }

            $account->products = $this->groupPetitions($account->petitions);
            $account->products_cant = count($account->products);

        }


        return $accounts;
    }

    public function formatPetitionsDetails($petitions, $currency)
    {
        if (count($petitions['extras']) > 0) {
            foreach ($petitions['extras'] as $extra) {
                $extra['price'] = Helper::number($extra['price'][$currency['iso']], $currency['decimals'], $currency['decimal_separator'], $currency['thousand_separator']);
            }
        }
        return $petitions;
    }

    private function statusAccount($status)
    {
        switch ($status) {
            case AccountStatusEnum::$open:
                $status = __('Abierta');
                break;
            case AccountStatusEnum::$paying:
                $status = __('Pagando');
                break;
            case AccountStatusEnum::$closed:
                $status = __('Cerrada');
                break;
            case AccountStatusEnum::$cancelled:
                $status = __('Cancelada');
                break;
        }

        return $status;
    }

    private function statusPetitions($status)
    {
        switch ($status) {
            case PetitionsStatusEnum::$pending:
                $status = __('Pendiente');
                break;
            case PetitionsStatusEnum::$in_progress:
                $status = __('En Progreso');
                break;
            case PetitionsStatusEnum::$preparing:
                $status = __('Preparando');
                break;
            case PetitionsStatusEnum::$to_deliver:
                $status = __('Por Entregar');
                break;
            case PetitionsStatusEnum::$delivered:
                $status = __('Entregado');
                break;
            case PetitionsStatusEnum::$cancelled:
                $status = __('Cancelado');
                break;
        }

        return $status;
    }

    private function formatCurrency($currency, $number)
    {
        $currency = Helper::json_object($currency);
        $number = Helper::number($number, $currency->decimals, $currency->decimal_separator, $currency->thousand_separator);
        return $number;
    }

    private function subTotal($petitions, $currency)
    {
        $amount_total = 0;
        foreach ($petitions as $petition) {
            if (PetitionsStatusEnum::$delivered == $petition->status_) {
                $amount_total = (float)$amount_total + $petition->amount;
            }
        }

        return $amount_total;
    }

    private function groupPetitions($petitions)
    {
        $group = [];
        $extra_id = '000';
        $contours_id = '000';
        $replaceables_id = '000';
        $extra_characteristic = '000';
        foreach ($petitions as $petition) {
            $details = Helper::json_object($petition->details);
            if ($details->characteristic) {
                $details_characteristic = Helper::object_array($details->characteristic);
                if (count($details_characteristic) > 0) {
                    foreach ($details_characteristic as $characteristic) {
                        if (count($characteristic['options']) > 0) {
                            foreach ($characteristic['options'] as $option) {
                                $extra_characteristic = $characteristic['id_characteristic'] . $characteristic['title_characteristic'] . $option['id'];
                            }
                        }
                    }
                } else {
                    $extra_characteristic = '000';
                }
            }

            if (count($details->extras) > 0) {
                foreach ($details->extras as $extra) {
                    $extra_id = $extra->id . $extra->name;
                }
            } else {
                $extra_id = '000';
            }


            if (isset($details->contours)) {
                if (count($details->contours) > 0) {
                    foreach ($details->contours as $contours) {
                        $contours_id = $contours->id . $contours->title;
                    }
                } else {
                    $contours_id = '000';
                }
            }

            if (isset($details->replaceables)) {
                if (count($details->replaceables) > 0) {
                    foreach ($details->replaceables as $replaceable) {
                        $replaceables_id = $replaceable->id . $replaceable->title;
                    }
                } else {
                    $replaceables_id = '000';
                }
            }

            $observation = isset($details->observation) ? $details->observation : '000';


            $uid = md5($petition->product . $extra_id . $contours_id . $replaceables_id . $extra_characteristic . $petition->status . $observation);

            if (isset($group[$uid])) {
                $group[$uid]['cant'] = $group[$uid]['cant'] + 1;
            } else {
                $group[$uid]['cant'] = 1;
            }

            $group[$uid] = [
                'cant' => $group[$uid]['cant'],
                'products' => $petition,
            ];

        }

        return $group;
    }

    private function details_invoice($details)
    {
        $details_ = Helper::json_object($details);
        $details = Helper::json_object($details_);

        if (isset($details->percentage_special) && count($details->percentage_special) > 0) {
            foreach ($details->percentage_special as $percentage_special) {
                $percentage_special->operator = $percentage_special->operator == 'subtract' ? '-' : '+';
            }
        }
        return $details;
    }

    public function formatDate($date)
    {
        $restaurant = Restaurants::find(session('restaurant.id'));
        $details = Helper::json_object($restaurant->details);
        $tz = isset($details->tz) ? $details->tz : $date['timezone'];
        $date_ = Helper::date($date['date'], $tz, 'd-m-Y');
        $hour = Helper::date($date['date'], $tz, 'H:i');
        $date = $date_ . ' - ' . $hour;
        return $date;
    }


}
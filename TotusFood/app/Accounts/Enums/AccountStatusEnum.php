<?php

namespace App\Accounts\Enums;

class AccountStatusEnum
{
    public static $open = 1;
    public static $paying = 2;
    public static $closed = 3;
    public static $cancelled = 4;
    public static $all = 5;
}
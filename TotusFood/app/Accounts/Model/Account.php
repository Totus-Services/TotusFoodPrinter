<?php

namespace App\Accounts\Model;

use App\Petitions\Model\Petitions;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'accounts';

    protected $fillable = ['id', 'account_id', 'waiter', 'client', 'currency', 'table', 'token_id', 'status', 'restaurant_id', 'details','created_at', 'updated_at'];


    public function petitions()
    {
        return $this->hasMany(Petitions::class, 'account_id', 'account_id');
    }
}

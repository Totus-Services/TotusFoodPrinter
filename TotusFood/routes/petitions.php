<?php

Route::group(['middleware' => [
    'cors',
],  'prefix' => 'petitions',], function () {


    Route::post('store', [
        'as' => 'petitions.store',
        'uses' => 'PetitionsController@store',
    ]);

    Route::post('change_order_account', [
        'as' => 'petitions.change',
        'uses' => 'PetitionsController@change_order_account',
    ]);

});
<?php

namespace App\Printer\Template;

use App\Helper;
use Carbon\Carbon;
use Mike42\Escpos\CapabilityProfiles\DefaultCapabilityProfile;
use Mike42\Escpos\Printer;

class Invoice
{
    public $printer;
    public $char_per_line = 0;
    public $currency_symbol = "";

    public function printer($contents, $connector)
    {
        try {
            $data = Helper::array_object($contents['data']);
            $this->char_per_line = $data->paper;
            $profile = DefaultCapabilityProfile::getInstance();
            $this->printer = new Printer($connector, $profile);
            $this->printer->selectCharacterTable(255);
            $textTable = Helper::validText(__("Mesa"));
            $textWaiter = Helper::validText(__("Mesero"));
            $textArea = Helper::validText(__("Área"));
            $textAccount = Helper::validText(__("Cuenta"));
            $textInvoice = Helper::validText(__("Recibo"));
            $textDate = Helper::validText(__("Fecha"));
            $textHour = Helper::validText(__("Hora"));
            $textProducts = Helper::validText(__("Productos"));
            $textProduct = Helper::validText(__("Producto"));
            $textCant = Helper::validText(__("Cant"));
            $textPrice = Helper::validText(__("Precio"));
            $client = Helper::validText(__("Cliente"));
            $email = Helper::validText(__("Email"));
            $phone = Helper::validText(__("Teléfono"));
            $address = Helper::validText(__("Dirección"));
            $textSubTotal = Helper::validText(__("SubTotal"));
            $textTotal = Helper::validText(__("Total"));
            $textTip = Helper::validText(__("Propina"));
            $textTax = ((int)$data->products->details->tax !== 0 ? Helper::validText(__("{$data->products->details->abbrev_tax}") . " ({$data->products->details->tax}%)") : null);
            $Tip = isset($data->products->details->tip) ? " ({$data->products->details->tip_percentage}%)" : null;
            $currency = str_replace(".", "", $data->products->currency->symbol);
            $Credits = Helper::validText(__("www.TotusFood.com"));
            $date = $data->head->dateTime->date;
            $hour = $data->head->dateTime->hour;
            // Head
            $this->printer->setTextSize(1, 2);
            $this->printer->text(Helper::centerText(Helper::validText(ucwords("{$data->head->title}")), $this->char_per_line) . "\n");
            $this->printer->setTextSize(1, 1);
            $this->printer->selectPrintMode(Printer::MODE_EMPHASIZED);
            $this->printer->text(Helper::centerText(Helper::validText(ucwords("{$data->head->subTitle}")), $this->char_per_line) . "\n");
            $this->printer->feed(1);
            $this->printer->setJustification();
            $this->printer->setTextSize(1, 1);
            $this->printer->selectPrintMode();
            $this->printer->text("{$textInvoice}: #{$data->head->invoice}\n");
            $this->printer->text("{$textAccount}: #{$data->head->account}\n");

            if (isset($data->head->area)) {
                $this->printer->text("{$textArea}: {$data->head->area}\n");
            }

            if (isset($data->head->table)) {
                $this->printer->text("{$textTable}: {$data->head->table}\n");
            }

            if (isset($data->head->waiter)) {
                $this->printer->text("{$textWaiter}: {$data->head->waiter}\n");
            }

            if (isset($data->products->details->user_data->dni) AND $data->products->details->user_data->dni) {
                $this->printer->text("{$data->products->details->user_data->type_dni}: {$data->products->details->user_data->dni}\n");
            }
            if (isset($data->products->details->user_data->fullname) AND $data->products->details->user_data->fullname) {
                $this->printer->text(Helper::validText("{$client}: {$data->products->details->user_data->fullname}\n"));
            }
            if (isset($data->products->details->user_data->email) AND $data->products->details->user_data->email) {
                $this->printer->text(Helper::validText("{$email}: {$data->products->details->user_data->email}\n"));
            }
            if (isset($data->products->details->user_data->phone) AND $data->products->details->user_data->phone) {
                $this->printer->text(Helper::validText("{$phone}: {$data->products->details->user_data->phone}\n"));
            }
            if (isset($data->products->details->user_data->address) AND $data->products->details->user_data->address) {
                $this->printer->text(Helper::validText("{$address}: {$data->products->details->user_data->address}\n"));
            }

            $this->printer->text($this->printLine("{$textDate}:{$date}^{$textHour}:{$hour}") . "\n");
            $this->printer->setJustification();
            $this->printer->selectPrintMode(Printer::JUSTIFY_LEFT);
            // end Head
            $this->printer->text($this->drawLine());
            $this->printer->selectPrintMode(Printer::MODE_EMPHASIZED);
            $this->printer->text($this->printLine("{$textCant}(x) {$textProduct}^{$textPrice}", $this->char_per_line) . "\n");
            $this->printer->selectPrintMode();
            $this->printer->text($this->drawLine());
            foreach ($data->products->products as $item) {
                $text_per_line = Helper::validText("{$item->cant}(x) {$item->name}^{$item->amount}");
                $text_per_line_max = Helper::validText("{$item->cant}(x) {$item->name}^");
                $subtotal = "{$item->amount}\n";
                $count_chars = strlen($text_per_line . 1);
                if ($count_chars > $this->char_per_line) {
                    $text_max = $text_per_line_max . $subtotal;
                    $length = strlen($text_max . 1);
                    $number_line = (int)ceil(round($length / $this->char_per_line, PHP_ROUND_HALF_UP));
                    $size = (int)$number_line * $this->char_per_line;
                    $this->printer->text($this->printLine($text_max, $size));

                } else {
                    $this->printer->text($this->printLine($text_per_line, $this->char_per_line) . "\n");
                }
                if ($item->with_extras == 'true') {
                    $this->printer->text("  -- (" . __("extras") . ")\n");
                }
            }
            $this->printer->text($this->drawLine());
            $this->printer->text($this->printLine("{$textSubTotal}^{$currency}{$data->products->amount}", $this->char_per_line) . "\n");
            if (isset($data->products->details->percentage_special) && count($data->products->details->percentage_special) > 0) {
                foreach ($data->products->details->percentage_special as $percentage_special) {
                    $operator = $percentage_special->operator == "sum" ? "+" : "-";
                    $this->printer->text($this->printLine("{$percentage_special->name} ({$operator}{$percentage_special->percentage}%)^{$currency}{$percentage_special->amount}", $this->char_per_line) . "\n");
                }
            }
            if (!is_null($Tip)) {
                $tip_tax = $data->products->details->tip_tax  == "true" ? "*" : "";
                $this->printer->text($this->printLine("{$textTip}{$Tip}{$tip_tax}^{$currency}{$data->products->details->tip}", $this->char_per_line) . "\n");
            }

            if (!is_null($textTax)) {
                $this->printer->text($this->printLine("{$textTax}^{$currency}{$data->products->details->tax_amount}", $this->char_per_line) . "\n");
            }
            $this->printer->text($this->drawLine());
            $this->printer->selectPrintMode(Printer::MODE_DOUBLE_HEIGHT);
            $this->printer->text(Helper::validText($this->printLine("{$textTotal}^{$currency}{$data->products->details->amount_total}", $this->char_per_line) . "\n"));
            $this->printer->feed();
            $this->printer->selectPrintMode();
            if($data->products->details->tip_tax == "true" AND $Tip !== null) {
                $this->printer -> setFont(Printer::FONT_C);
                $this->printer->text(Helper::validText(__("*"). __("Propina incluida en calculo de impuesto.")));
                $this->printer -> setFont();
                $this->printer->feed();
            }
            $this->printer->feed();
            $this->printer->text(Helper::validText(Helper::centerText($Credits, $this->char_per_line)));
            $this->printer->feed();
            $this->printer->feed();
            $this->printer->feed();
            $this->printer->feed();
            $this->printer->cut();
            $this->printer->close();
            $response = [
                'title' => __('¡Listo!'),
                'status' => 200,
                'message' => __("Recibo #{$data->head->invoice}, impreso correctamente"),

            ];
        } catch (\Exception $exception) {
            $response = [
                'title' => __('¡Error!'),
                'status' => 500,
                'message' => __('Error imprimiendo, por favor verifica el estado de impresora, alimentación de energía, puertos de conexión e intente nuevamente') . "."
            ];
        }

        return $response;
    }

    public function open_drawer()
    {
        $this->printer->pulse();
        $this->printer->close();
    }

    function drawLine()
    {
        $new = '';
        for ($i = 1; $i < $this->char_per_line; $i++) {
            $new .= '-';
        }
        return $new . "\n";

    }

    function printLine($str, $size = NULL, $sep = "^", $space = NULL)
    {
        if (!$size) {
            $size = $this->char_per_line;
        }
        $size = $space ? $space : $size;
        $length = strlen($str);
        list($first, $second) = explode("^", $str, 2);
        $line = $first . ($sep == "^" ? $sep : '');
        for ($i = 1; $i < ($size - $length); $i++) {
            $line .= ' ';
        }
        $line .= ($sep != "^" ? $sep : '') . $second;
        $line = str_replace("^", " ", $line);
        return $line;
    }


}
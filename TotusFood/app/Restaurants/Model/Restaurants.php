<?php

namespace App\Restaurants\Model;

use Illuminate\Database\Eloquent\Model;

class Restaurants extends Model
{
    protected $table = 'restaurants';

    protected $fillable = ['id', 'name', 'owner', 'country', 'token', 'details', 'remember', 'current', 'created_at', 'updated_at'];

}

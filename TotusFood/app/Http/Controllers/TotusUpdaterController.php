<?php

namespace App\Http\Controllers;

use App\Helper;
use App\Printer\Collection\PrinterCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class TotusUpdaterController extends Controller
{
    private $tmp_backup_dir = null;

    /*
    * Download and Install Update.
    */
    public function update(Request $request, PrinterCollection $printerCollection)
    {

        $archive = $request->get('archive');
        $lastVersionInfo = $this->getLastVersion($archive);

//        if ( $lastVersionInfo['version'] <= $this->getCurrentVersion() ){
//            $response = [
//                'title' => __('Actualizado'),
//                'message' => __("¡Su sistema ya está actualizado a la última versión!"),
//                'status' => 200,
//            ];
//            return $response;
//        }

        try{
            $this->tmp_backup_dir = base_path().'/backup_'.date('Ymd');
            $update_path = null;
            if( ($update_path = $this->download($lastVersionInfo['archive'])) === false)
                throw new \Exception("Error during download.");

//            Artisan::call('down');
            $status = $this->install($lastVersionInfo['version'], $update_path, $lastVersionInfo['archive']);
            if($status['status'] === true){
                $this->setCurrentVersion($lastVersionInfo['version']); //update system version
                Artisan::call('up'); //restore system UP status

                $changelog = Helper::array_object($this->changelog());
                $logs = $printerCollection->formatChangelog($changelog);

                $response = [
                    'title' => __('Totus Food Printer se ha actualizado'),
                    'message' => __("Versión ") . ": " . $lastVersionInfo['version'],
                    'changelog' => $logs,
                    'version'   =>  $lastVersionInfo['version'],
                    'status' => 200,
                ];
            }else
                throw new \Exception("Error during updating.");

        }catch (\Exception $e) {

            $response = [
                'title' => __('Error actualizando'),
                'message' => $e,
                'status' => 500,
            ];
            $this->restore();
        }
        return $response;
    }

    private function install($lastVersion, $update_path, $archive)
    {

        try{
            $execute_commands = false;
            $upgrade_cmds_filename = 'upgrade.php';
            $upgrade_cmds_path = config('laraupdater.tmp_path').'/'.$upgrade_cmds_filename;

            $zipHandle = zip_open($update_path);
            $archive = substr($archive,0, -4);
            File::copy(base_path('/changelog/changelog.txt'), base_path('/changelog/changelog-copy.txt'));

            $files = [];
            $directory = [];
            $msg = '';

            while ($zip_item = zip_read($zipHandle) ){
                $filename = zip_entry_name($zip_item);
                $dirname = dirname($filename);

                // Exclude these cases (1/2)
                if(	substr($filename,-1,1) == '/' || dirname($filename) === $archive || substr($dirname,0,2) === '__') continue;

                //Exclude root folder (if exist)

                if( substr($dirname,0, strlen($archive)) === $archive )
                    $dirname = substr($dirname, (strlen($dirname)-strlen($archive)-1)*(-1));
//                 Exclude these cases (2/2)
//                if($dirname === '.' ) continue;

                $filename = $dirname.'/'.basename($filename); //set new purify path for current file

                if ( !is_dir(base_path() . '/' . $dirname) ){ //Make NEW directory (if exist also in current version continue...)
                    File::makeDirectory(base_path().'/'.$dirname, $mode = 0755, true, true);
                    $directory[] = $dirname;
                }

                if ( !is_dir(base_path().'/'.$filename) ){ //Overwrite a file with its last version
                    $contents = zip_entry_read($zip_item, zip_entry_filesize($zip_item));
                    $contents = str_replace("\r\n", "\n", $contents);
                    if ( strpos($filename, 'upgrade.php') !== false ) {
                        File::put($upgrade_cmds_path, $contents);
                        $execute_commands = true;

                    }else {
                        $files[] = $filename;
                        if(File::exists(base_path().'/'.$filename)) $this->backup($filename); //backup current version
                        File::put(base_path().'/'.$filename, $contents);

                        unset($contents);
                    }

                }
            }
            zip_close($zipHandle);

            if($execute_commands == true){
                include ($upgrade_cmds_path);

                if(main()) //upgrade-VERSION.php contains the 'main()' method with a BOOL return to check its execution.
                    $msg = __('Comandos ejecutados con éxito');
                else
                $msg = __('Error durante la ejecución de los comandos');
                unlink($upgrade_cmds_path);
                File::delete($upgrade_cmds_path); //clean TMP
            }
              File::delete($update_path); //clean TMP
              File::deleteDirectory($this->tmp_backup_dir); //remove backup tmp_path folder

            $response = [
                'title' => __('success'),
                'message' => $msg,
                'directory' => $directory,
                'files' => $files,
                'status' => true,
            ];

        }catch (\Exception $e) {

            $response = [
                'title' => __('error'),
                'message' => $e,
                'status' => false,
            ];
        }
        return $response;
    }

    /*
    * Download Update from $update_baseurl to $tmp_path (local folder).
    */
    private function download($update_name)
    {
        try{
            $filename_tmp = storage_path('tmp_path').'/'.$update_name;

            if ( !is_file( $filename_tmp ) ) {
                $newUpdate = file_get_contents(config('laraupdater.update_baseurl').'/'.$update_name);
                $dlHandler = fopen($filename_tmp, 'w');
                if ( !fwrite($dlHandler, $newUpdate) ){
                    echo '<p>Could not save new update (check tmp/ write permission). Update aborted.</p>';
                    exit();
                }
            }

        }catch (\Exception $e) { return false; }

        return $filename_tmp;
    }

    /*
    * Return current version (as plain text).
    */
    public function getCurrentVersion(){
        $version = File::get(base_path().'/version.txt');
        return $version;
    }

    /*
    * Check if a new Update exist.
    */
    public function check($content)
    {
        $lastVersionInfo = $this->getLastVersion($content);
        if( version_compare($lastVersionInfo['version'], $this->getCurrentVersion(), ">") )
            return $lastVersionInfo['version'];

        return '';
    }

    private function setCurrentVersion($last){
        File::put(base_path().'/version.txt', $last); //UPDATE $current_version to last version
    }

    private function getLastVersion($content){
//        $content = file_get_contents(config('laraupdater.update_baseurl').'/laraupdater.json');
//        $content = json_decode($content, true);
        return $content; //['version' => $v, 'archive' => 'RELEASE-$v.zip', 'description' => 'plain text...'];
    }

    private function backup($filename){
        $backup_dir = $this->tmp_backup_dir;

        if ( !is_dir($backup_dir) ) File::makeDirectory($backup_dir, $mode = 0755, true, true);
        if ( !is_dir($backup_dir.'/'.dirname($filename)) ) File::makeDirectory($backup_dir.'/'.dirname($filename), $mode = 0755, true, true);
        File::copy(base_path().'/'.$filename, $backup_dir.'/'.$filename); //to backup folder
    }

    private function restore() {
        if( !isset($this->tmp_backup_dir) )
            $this->tmp_backup_dir = base_path().'/backup_'.date('Ymd');

        try{
            $backup_dir = $this->tmp_backup_dir;
            $backup_files = File::allFiles($backup_dir);

            foreach ($backup_files as $file){
                $filename = (string)$file;
                $filename = substr($filename, (strlen($filename)-strlen($backup_dir)-1)*(-1));
                echo $backup_dir.'/'.$filename." => ".base_path().'/'.$filename;
                File::copy($backup_dir.'/'.$filename, base_path().'/'.$filename); //to respective folder
            }

        }catch(\Exception $e) {
            echo "Exception => ".$e->getMessage();
            echo "<BR>[ FAILED ]";
            echo "<BR> Backup folder is located in: <i>".$backup_dir."</i>.";
            echo "<BR> Remember to restore System UP-Status through shell command: <i>php artisan up</i>.";
            return false;
        }

        echo "[ RESTORED ]";
        return true;
    }

    private function changelog() {
        $changelog = File::get(base_path('changelog/changelog.txt'));
        $str =str_replace("\n","",$changelog);

        $version = $this->extractText($str,'## Version - [',']');
        $date = $this->extractText($str,'## Date - [',']');

        $add = $this->extractText($str,'### Added','### Changed');
        $changed = $this->extractText($str,'### Changed','### Removed');
        $removed = $this->extractText($str,'### Removed','### End');


        $add_list = preg_split('/(-\s)+/', $add);
        unset($add_list[0]);
        $changed_list = preg_split('/(-\s)+/', $changed);
        unset($changed_list[0]);
        $removed_list = preg_split('/(-\s)+/', $removed);
        unset($removed_list[0]);

        $response = [
            'version' => $version,
            'date' => $date,
            'logs' => [
                'add' => $add_list,
                'changed' => $changed_list,
                'removed' => $removed_list
            ]
        ];

        $changelog_ =  Helper::array_object($response['logs']);
        $logs = json_encode (Helper::object_json($changelog_));

        DB::table('changelog')->insert([
            'version' => $version,
            'date' => $date,
            'log' => $logs
        ]);

        return $response;
    }

    private function extractText($text,$first,$last){
        $r = explode($first, $text);
        if (isset($r[1])){
            $r = explode($last, $r[1]);
            return $r[0];
        }
        return '';
    }




}

var Printer;
Printer = function () {
    return {
        init: function (settings, token) {
            $("#logout").click(function () {
                $logout = $("#logout");
                swal({
                    title: settings.titleLogout,
                    text: settings.message,
                    type: "warning",
                    confirmButtonText: settings.buttons.confirmText,
                    cancelButtonText: settings.buttons.cancelText,
                    cancelButtonColor: '#b32828',
                    reverseButtons: true,
                    showCancelButton: true
                }).then(function (isConfirm) {
                    if (isConfirm.value) {
                        $('#preload').show();
                        $.ajax({
                            url: $logout.data('logout-api'),
                            type: "POST",
                            data: {
                                restaurant: $logout.data('restaurant'),
                                token: $logout.data('token')
                            },
                            success: function (data) {
                                if (data.status === 200) {
                                    $.ajax({
                                        url: $logout.data('url'),
                                        type: "POST",
                                        data: {},
                                        success: function (data) {
                                            if (data.status === 'OK') {
                                                window.location.href = "/";
                                                $('#preload').hide();
                                            }
                                        }
                                    });
                                }
                            }
                        })
                    }
                })
            })
        },
        auth: function (lang) {
            $("#login").click(function () {
                var loginForm = $("#loginForm");
                var validation = loginForm.parsley().validate();
                var remember = $('#remember').val();
                if (validation) {
                    var btnLogin = $(this).button('loading');
                    $('#preload').show();
                    var restaurant = $("#restaurant");
                    if (restaurant.val() > 0) {
                        $.ajax({
                            url: restaurant.data('restaurant'),
                            type: "POST",
                            data: {restaurant: restaurant.val()},
                            success: function (data) {
                                Printer.logIn(data, btnLogin, lang, remember);
                            }
                        });
                    } else {
                        $.ajax({
                            url: loginForm.data('url'),
                            type: "POST",
                            data: loginForm.serialize(),
                            success: function (data) {
                                if (data.type === env.roles.owner) {
                                    $("#username").attr("disabled", true);
                                    $("#password").attr("disabled", true);
                                    $("#restaurant").removeClass("hidden");
                                    $("#restaurant").attr("required", true);
                                    $.each(data.response, function (index, value) {
                                        $('<option></option>').attr('value', value.id).text(value.name).select(index).appendTo("#restaurant");
                                    });
                                    btnLogin.button('reset');
                                    $('#preload').hide();
                                }

                                else if (data.type === env.roles.super_admin) {
                                    btnLogin.button('reset');
                                    $('#preload').hide();
                                    $.post("dashboard/root", {
                                        'response': data.response
                                    }, function (res) {
                                        toastrPersonalized.toastr(res.title, res.message, 'success');
                                        if (res) {
                                            setTimeout(function () {
                                                window.location.href = "/";
                                                $('#preload').hide();
                                            }, 2000);
                                        }

                                    });
                                }
                                else {
                                    Printer.logIn(data, btnLogin, lang, remember);
                                    loginForm.parsley().reset();
                                }
                            }
                        });
                    } // end restaurant
                } // end validation
                return false;
            });

        },  // Function Log In
        logIn: function (data, btnLogin, lang, remember) {
            if (data.status === env.status.ok) {
                $('#preload').show();
                purchased = data.response.module_printer_purchased === true;
                data.response.remember = remember;
                data.response.purchased = purchased;
                $.post("dashboard", {
                    'response': data.response
                }, function (res) {
                    toastrPersonalized.toastr(res.title, res.message, 'success');
                    if (res) {
                        setTimeout(function () {
                            window.location.href = "/";
                            $('#preload').hide();
                        }, 3000);
                    }

                });
                return false;
                btnLogin.button('reset');
                $("#username").removeAttr("disabled");
                $("#password").removeAttr("disabled");
                $('#preload').hide();
                return false;
            } else {
                toastrPersonalized.toastr('', data.message, 'error');
                btnLogin.button('reset');
                $('#preload').hide();
                return false;
            }
        },
        logOut: function (lang) {

        },
        loginRemember: function (settings, btn) {
            id = $('#restaurant-id').val();
            btn.button('loading');
            $.post(settings.url + 'printers/restaurant/token', {id: id}, function (data) {
                $.post('dashboard', {response: data}, function (res) {
                    if (res) {
                        btn.button('reset');
                        window.location.href = "/";
                        $('#preload').hide();
                    }
                });
            });
        }
    }

}();
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>{{ $title }}</title>
    <style type="text/css">
        body {
            font: 12pt Georgia, "Times New Roman", Times, serif;
            line-height: 1.3;
            padding-top: 20px;
        }

        div.header {
            display: block;
            text-align: center;
            position: running(header);
            width: 100%;
        }

        div.footer {
            display: block;
            text-align: center;
            position: running(footer);
            width: 100%;
        }
        
        .title {
            text-align: center;
        }

        .accounts {
            border: 1px solid #9b9b9b;;
            margin: 25px 0;
        }

        .table-header {
            color: #575757;
        }


        .text-left { text-align: left;}
        .text-right { text-align: right;}
        .text-center { text-align: center;}
        .text-justify { text-align: justify;}

        .col-1 {width: 8.33%;}
        .col-2 {width: 16.66%;}
        .col-3 {width: 25%;}
        .col-4 {width: 33.33%;}
        .col-5 {width: 41.66%;}
        .col-6 {width: 50%;}
        .col-7 {width: 58.33%;}
        .col-8 {width: 66.66%;}
        .col-9 {width: 75%;}
        .col-10 {width: 83.33%;}
        .col-11 {width: 91.66%;}
        .col-12 {width: 100%;}


        @page {
            /* switch to landscape */
            size: landscape; /* set page margins */
            margin: 0.5cm;

        }

        @top-center
        {
            content: element(header);
        }
        @bottom-center
        {
            content: element(footer);
        }
        @bottom-right
        {
            content: counter(page) " of " counter(pages);
        }

        
        .custom-page-start {
            margin-top: 50px;
        }

        table {  color: #333; font-family: Helvetica, Arial, sans-serif; width: 100%; border-collapse: collapse;}

        td, th { border: none; height: 30px; }

        th { background: #D3D3D3; font-weight: bold; }

        td { background: #FAFAFA; text-align: center; }

        tr:nth-child(even) td { background: #F1F1F1; }

        tr:nth-child(odd) td { background: #FEFEFE; }

        ul > li {
            list-style: none;
            padding: 0 !important;
        }

        ul {
            padding: 0 !important;
            margin: 3px;
        }


        .table-header {
            margin: 0 0 15px 0;
            padding: 0 15px;
        }
        .table-header tr:nth-child(even) td {
            background: #FEFEFE !important;
        }

        .table-header tr:nth-child(odd) td {
            background: #FEFEFE !important;
        }

        .table-petitions td,
        .table-petitions th { border: 1px solid #D3D3D3; height: 30px; }

        .table-totals {
            padding: 0 5px 0 15px;
            margin: 0;
            font-size: 12pt;
        }

        .table-totals tr:nth-child(even) td {
            background: #FEFEFE !important;
        }

        .table-totals tr:nth-child(odd) td {
            background: #FEFEFE !important;
        }

        .amounts {
            padding: 0 5px 0 0;
        }

        .total {
            font-size: 16pt;
            font-weight: bold;
        }

        .empty {
            font-size: 18pt;
            font-weight: bold;
            padding: 20px;
        }

        .status {
            font-size: 11pt;
        }

        .is-disable {
            color: rgba(85, 85, 85, 0.43);
        }

        .is-delivered {
            font-weight: 600;
        }


    </style>
</head>
<body>
<!-- Custom HTML header -->
<div class="header">
    <div class="title">
        <h1>{{ $restaurant->name }}</h1>
    </div>

</div>
<h3 class="text-center">{{ $title }}</h3>
@if(count($accounts) > 0)
    @foreach($accounts as $account)
        <div class="accounts">
            <table class="table table-header" width="100%">
                <tbody>
                <tr>
                    <td width="50%" class="text-left"><span><strong>{{ __('Cuenta') }}
                                :</strong> #{{ $account['token_id'] }}</span></td>
                    <td width="50%" class="text-right"><strong>{{ __('Estado de cuenta') }}
                            :</strong> {{ $account['status'] }}</td>
                </tr>
                <tr>
                    <td width="50%" class="text-left"><span><strong>{{ __('Mesero') }}
                                :</strong> {{ $account['waiter'] }}</span>
                    </td>
                    <td width="50%" class="text-right"><strong>{{ __('Mesa') }}:</strong> #{{ $account['table'] }}</td>
                </tr>

                <tr>
                    @if(isset($account['client']))
                        <td width="50%" class="text-left"><span><strong>{{ __('Cliente') }}:</strong> {{ $account['client'] }}</span></td>
                    @else
                        <td width="50%" class="text-left"></td>
                    @endif
                    <td width="50%" class="text-right"><span><strong>{{ __('Fecha') }}:</strong> {{ $account['date'] }}</span>
                </tr>
                </tbody>
            </table>
            <table class="table table-petitions" width="100%">
                @if(count($account['petitions']) > 0)
                    <thead>
                    <tr>
                        <th width="2%" class="text-center">{{ __('Cantidad') }}</th>
                        <th width="35%" class="text-center">{{ __('Producto') }}</th>
                        <th width="13%" class="text-center">{{ __('Estatus') }}</th>
                        <th width="25%" class="text-center">{{ __('Precio') }}</th>
                        <th width="25%" class="text-right amounts">{{ __('Monto') }}</th>
                    </tr>
                    </thead>
                    @foreach($account['products'] as $petitions)
                        <tbody>
                        <tr>
                            <td width="3%">{{ $petitions['cant'] }}</td>
                            <td width="35%">
                                {{ $petitions['products']['product'] }}
                                {{--Extras--}}
                                @if(count($petitions['products']['details']['extras']) > 0)
                                    <ul>
                                        <li><strong>{{ __('Extras') }}</strong></li>
                                    </ul>
                                    @foreach($petitions['products']['details']['extras'] as $extra)
                                        <ul>
                                            <li>{{ $extra['name'] }}</li>
                                        </ul>
                                    @endforeach
                                @endif
                                {{--Characteristic--}}
                                @if(count($petitions['products']['details']['characteristic']) > 0)
                                    @foreach($petitions['products']['details']['characteristic'] as $characteristic)
                                        <ul>
                                            <li><strong>{{ $characteristic['title_characteristic'] }}</strong></li>
                                        </ul>
                                        @foreach($characteristic['options'] as $options)
                                            <ul>
                                                <li>{{ $options['name'] }}</li>
                                            </ul>
                                        @endforeach
                                    @endforeach
                                @endif

                                {{-- Contours --}}
                                @if(isset($petitions['products']['details']['contours']))
                                    @if(count($petitions['products']['details']['contours']) > 0)
                                        <ul>
                                            <li><strong>{{ __('Contornos') }}</strong></li>
                                        </ul>
                                        @foreach($petitions['products']['details']['contours'] as $options)
                                            <ul>
                                                <li>{{ $options['title'] }}</li>
                                            </ul>
                                        @endforeach
                                    @endif
                                @endif


                                {{-- Remplaceables --}}
                                @if(isset($petitions['products']['details']['replaceables']))
                                    @if(count($petitions['products']['details']['replaceables']) > 0)
                                        <ul>
                                            <li><strong>{{ __('Reemplazables') }}</strong></li>
                                        </ul>
                                        @foreach($petitions['products']['details']['replaceables'] as $options)
                                            <ul>
                                                <li>{{ $options['title'] }}</li>
                                            </ul>
                                        @endforeach
                                    @endif
                                @endif

                                {{-- Ingredients --}}
                                @if(isset($petitions['products']['details']['ingredients']))
                                    @if(count($petitions['products']['details']['ingredients']) > 0)
                                        <ul>
                                            <li><strong>{{ __('Contornos') }}</strong></li>
                                        </ul>
                                        @foreach($petitions['products']['details']['ingredients'] as $options)
                                            <ul>
                                                <li>{{ $options['title'] }}</li>
                                            </ul>
                                        @endforeach
                                    @endif
                                @endif

                                {{-- Ingredients --}}
                                @if(isset($petitions['products']['details']['observation']))
                                    <ul>
                                        <li><strong>{{ __('Observación') }}</strong></li>
                                    </ul>
                                    <ul>
                                        <li>{{ $petitions['products']['details']['observation'] }}</li>
                                    </ul>
                                @endif

                            </td>
                            <td width="13%" class="status">
                                {{ $petitions['products']['status'] }}
                                {{--Extras--}}
                                @if(count($petitions['products']['details']['extras']) > 0)
                                    <ul>
                                        @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                            <li class="is-delivered"><strong>---</strong></li>
                                        @else
                                            <li class="is-disable"><strong>---</strong></li>
                                        @endif
                                    </ul>
                                    @foreach($petitions['products']['details']['extras'] as $extra)
                                        <ul>
                                            <li>{{ $petitions['products']['status'] }}</li>
                                        </ul>
                                    @endforeach
                                @endif

                                {{--Characteristic--}}
                                @if(count($petitions['products']['details']['characteristic']) > 0)
                                    @foreach($petitions['products']['details']['characteristic'] as $characteristic)
                                        <ul>
                                            @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                <li class="is-delivered"><strong>---</strong></li>
                                            @else
                                                <li class="is-disable"><strong>---</strong></li>
                                            @endif
                                        </ul>
                                        @foreach($characteristic['options'] as $options)
                                            <ul>
                                                <li>{{ $petitions['products']['status'] }}</li>
                                            </ul>
                                        @endforeach
                                    @endforeach
                                @endif

                                {{--Contours--}}
                                @if(isset($petitions['products']['details']['contours']))
                                    @if(count($petitions['products']['details']['contours']) > 0)
                                        <ul>
                                            @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                <li class="is-delivered"><strong>---</strong></li>
                                            @else
                                                <li class="is-disable"><strong>---</strong></li>
                                            @endif
                                        </ul>
                                        @foreach($petitions['products']['details']['contours'] as $contours)
                                            <ul>
                                                <li>{{ $petitions['products']['status'] }}</li>
                                            </ul>
                                        @endforeach
                                    @endif
                                @endif

                                {{--Replaceables--}}
                                @if(isset($petitions['products']['details']['replaceables']))
                                    @if(count($petitions['products']['details']['replaceables']) > 0)
                                        <ul>
                                            @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                <li class="is-delivered"><strong>---</strong></li>
                                            @else
                                                <li class="is-disable"><strong>---</strong></li>
                                            @endif
                                        </ul>
                                        @foreach($petitions['products']['details']['replaceables'] as $replaceables)
                                            <ul>
                                                <li>{{ $petitions['products']['status'] }}</li>
                                            </ul>
                                        @endforeach
                                    @endif
                                @endif

                                {{--Ingredients--}}
                                @if(isset($petitions['products']['details']['ingredients']))
                                    @if(count($petitions['products']['details']['ingredients']) > 0)
                                        <ul>
                                            @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                <li class="is-delivered"><strong>---</strong></li>
                                            @else
                                                <li class="is-disable"><strong>---</strong></li>
                                            @endif
                                        </ul>
                                        @foreach($petitions['products']['details']['ingredients'] as $ingredients)
                                            <ul>
                                                <li>{{ $petitions['products']['status'] }}</li>
                                            </ul>
                                        @endforeach
                                    @endif
                                @endif

                                @if(isset($petitions['products']['details']['observation']))
                                    <ul>
                                        @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                            <li class="is-delivered"><strong>---</strong></li>
                                        @else
                                            <li class="is-disable"><strong>---</strong></li>
                                        @endif
                                    </ul>
                                    <ul>
                                        <li>{{ $petitions['products']['status'] }}</li>
                                    </ul>
                                @endif

                            </td>

                            @if($petitions['products']['cancelled'] == true)
                                <td width="25%" class="text-right amounts">

                                    <strike>{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($petitions['products']['amount'],$account['currency'] ) }}</strike>
                                    {{--Extras--}}
                                    @if(count($petitions['products']['details']['extras']) > 0)
                                        <ul>
                                            <li><strong>{{ __('Extras') }}</strong></li>
                                        </ul>
                                        @foreach($petitions['products']['details']['extras'] as $extra)
                                            <ul>
                                                @if($petitions['products']['status_'] == '5')
                                                    <li class="is-delivered">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($petitions['products']['amount'],$account['currency'] ) }}</li>
                                                @else
                                                    <li class="is-disable">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($petitions['products']['amount'],$account['currency'] ) }}</li>
                                                @endif
                                            </ul>
                                        @endforeach
                                    @endif

                                    {{--Characteristic--}}
                                    @if(count($petitions['products']['details']['characteristic']) > 0)
                                        @foreach($petitions['products']['details']['characteristic'] as $characteristic)
                                            <ul>
                                                @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                    <li class="is-delivered"><strong>---</strong></li>
                                                @else
                                                    <li class="is-disable"><strong>---</strong></li>
                                                @endif
                                            </ul>
                                            @foreach($characteristic['options'] as $options)
                                                <ul>
                                                    @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                        <li class="is-delivered">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($options['price'],$account['currency'] ) }}</li>
                                                    @else
                                                        <li class="is-disable">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($options['price'],$account['currency'] ) }}</li>
                                                    @endif
                                                </ul>
                                            @endforeach
                                        @endforeach
                                    @endif

                                    {{--Contours--}}
                                    @if(isset($petitions['products']['details']['contours']))
                                        @if(count($petitions['products']['details']['contours']) > 0)
                                            <ul>
                                                @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                    <li class="is-delivered"><strong>---</strong></li>
                                                @else
                                                    <li class="is-disable"><strong>---</strong></li>
                                                @endif
                                            </ul>
                                            @foreach($petitions['products']['details']['contours'] as $contours)
                                                @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                    <li class="is-delivered"><strong>---</strong></li>
                                                @else
                                                    <li class="is-disable"><strong>---</strong></li>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endif

                                    {{--Replaceables--}}
                                    @if(isset($petitions['products']['details']['replaceables']))
                                        @if(count($petitions['products']['details']['replaceables']) > 0)
                                            <ul>
                                                @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                    <li class="is-delivered"><strong>---</strong></li>
                                                @else
                                                    <li class="is-disable"><strong>---</strong></li>
                                                @endif
                                            </ul>
                                            @foreach($petitions['products']['details']['replaceables'] as $replaceables)
                                                @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                    <li class="is-delivered"><strong>---</strong></li>
                                                @else
                                                    <li class="is-disable"><strong>---</strong></li>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endif

                                    {{--Ingredients--}}
                                    @if(isset($petitions['products']['details']['ingredients']))
                                        @if(count($petitions['products']['details']['ingredients']) > 0)
                                            <ul>
                                                @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                    <li class="is-delivered"><strong>---</strong></li>
                                                @else
                                                    <li class="is-disable"><strong>---</strong></li>
                                                @endif
                                            </ul>
                                            @foreach($petitions['products']['details']['ingredients'] as $ingredients)
                                                @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                    <li class="is-delivered"><strong>---</strong></li>
                                                @else
                                                    <li class="is-disable"><strong>---</strong></li>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endif

                                    @if(isset($petitions['products']['details']['observation']))
                                        <ul>
                                            @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                <li class="is-delivered"><strong>---</strong></li>
                                            @else
                                                <li class="is-disable"><strong>---</strong></li>
                                            @endif
                                        </ul>
                                        @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                            <li class="is-delivered"><strong>---</strong></li>
                                        @else
                                            <li class="is-disable"><strong>---</strong></li>
                                        @endif
                                    @endif
                                </td>

                                <td width="25%" class="text-right amounts">
                                    <strike>{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($petitions['products']['amount'],$account['currency'] ) }}</strike>
                                    {{--Extras--}}
                                    @if(count($petitions['products']['details']['extras']) > 0)
                                        <ul>
                                            @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                <li class="is-delivered"><strong>---</strong></li>
                                            @else
                                                <li class="is-disable"><strong>---</strong></li>
                                            @endif
                                        </ul>
                                        @foreach($petitions['products']['details']['extras'] as $extra)
                                            <ul>
                                                @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                    <li class="is-delivered">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($petitions['products']['amount'],$account['currency'] ) }}</li>
                                                @else
                                                    <li class="is-disable">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($petitions['products']['amount'],$account['currency'] ) }}</li>
                                                @endif
                                            </ul>
                                        @endforeach
                                    @endif

                                    {{--Characteristic--}}
                                    @if(count($petitions['products']['details']['characteristic']) > 0)
                                        @foreach($petitions['products']['details']['characteristic'] as $characteristic)
                                            <ul>
                                                @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                    <li class="is-delivered"><strong>---</strong></li>
                                                @else
                                                    <li class="is-disable"><strong>---</strong></li>
                                                @endif
                                            </ul>
                                            @foreach($characteristic['options'] as $options)
                                                <ul>
                                                    @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                        <li class="is-delivered">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($options['price'],$account['currency'] ) }}</li>
                                                    @else
                                                        <li class="is-disable">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($options['price'],$account['currency'] ) }}</li>
                                                    @endif
                                                </ul>
                                            @endforeach
                                        @endforeach
                                    @endif
                                </td>

                            @else
                                <td width="25%" class="text-right amounts">
                                    @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                    <span class="is-delivered">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($petitions['products']['details']['amount'],$account['currency'] ) }}</span>
                                    @else
                                        <span class="is-disable">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($petitions['products']['details']['amount'],$account['currency'] ) }}</span>
                                    @endif
                                    {{--Extras--}}
                                    @if(count($petitions['products']['details']['extras']) > 0)
                                        <ul>
                                            @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                <li class="is-delivered" style="margin-right: 8%"><strong>---</strong></li>
                                            @else
                                                <li class="is-disable" style="margin-right: 8%"><strong>---</strong></li>
                                            @endif
                                        </ul>
                                        @foreach($petitions['products']['details']['extras'] as $extra)
                                            <ul>
                                                @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                    <li class="is-delivered">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($extra['price'][$account['currency']['iso']],$account['currency'] ) }}</li>
                                                @else
                                                    <li class="is-disable">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($extra['price'][$account['currency']['iso']],$account['currency'] ) }}</li>
                                                @endif
                                            </ul>
                                        @endforeach
                                    @endif

                                    {{--Characteristic--}}
                                    @if(count($petitions['products']['details']['characteristic']) > 0)
                                        @foreach($petitions['products']['details']['characteristic'] as $characteristic)
                                            <ul>
                                                @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                    <li class="is-delivered" style="margin-right: 8%"><strong>---</strong></li>
                                                @else
                                                    <li class="is-disable" style="margin-right: 8%"><strong>---</strong></li>
                                                @endif
                                            </ul>
                                            @foreach($characteristic['options'] as $options)
                                                <ul>
                                                    @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                        <li class="is-delivered">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($options['price'],$account['currency'] ) }}</li>
                                                    @else
                                                        <li class="is-disable">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($options['price'],$account['currency'] ) }}</li>
                                                    @endif
                                                </ul>
                                            @endforeach
                                            @endforeach
                                        @endif

                                        {{--Contours--}}
                                        @if(isset($petitions['products']['details']['contours']))
                                            @if(count($petitions['products']['details']['contours']) > 0)
                                                <ul>
                                                    @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                        <li class="is-delivered"><strong>---</strong></li>
                                                    @else
                                                        <li class="is-disable"><strong>---</strong></li>
                                                    @endif
                                                </ul>
                                                @foreach($petitions['products']['details']['contours'] as $contours)
                                                    <ul>
                                                        @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                            <li class="is-delivered"><strong>---</strong></li>
                                                        @else
                                                            <li class="is-disable"><strong>---</strong></li>
                                                        @endif
                                                    </ul>
                                                @endforeach
                                            @endif
                                        @endif

                                        {{--Replaceables--}}
                                        @if(isset($petitions['products']['details']['replaceables']))
                                            @if(count($petitions['products']['details']['replaceables']) > 0)
                                                <ul>
                                                    @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                        <li class="is-delivered"><strong>---</strong></li>
                                                    @else
                                                        <li class="is-disable"><strong>---</strong></li>
                                                    @endif
                                                </ul>
                                                @foreach($petitions['products']['details']['replaceables'] as $replaceables)
                                                    <ul>
                                                        @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                            <li class="is-delivered"><strong>---</strong></li>
                                                        @else
                                                            <li class="is-disable"><strong>---</strong></li>
                                                        @endif
                                                    </ul>
                                                @endforeach
                                            @endif
                                        @endif

                                        {{--Ingredients--}}
                                        @if(isset($petitions['products']['details']['ingredients']))
                                            @if(count($petitions['products']['details']['ingredients']) > 0)
                                                <ul>
                                                    @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                        <li class="is-delivered"><strong>---</strong></li>
                                                    @else
                                                        <li class="is-disable"><strong>---</strong></li>
                                                    @endif
                                                </ul>
                                                @foreach($petitions['products']['details']['ingredients'] as $ingredients)
                                                    <ul>
                                                        @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                            <li class="is-delivered"><strong>---</strong></li>
                                                        @else
                                                            <li class="is-disable"><strong>---</strong></li>
                                                        @endif
                                                    </ul>
                                                @endforeach
                                            @endif
                                        @endif

                                        @if(isset($petitions['products']['details']['observation']))
                                            <ul>
                                                @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                    <li class="is-delivered"><strong>---</strong></li>
                                                @else
                                                    <li class="is-disable"><strong>---</strong></li>
                                                @endif
                                            </ul>
                                            <ul>
                                            @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                <li class="is-delivered"><strong>---</strong></li>
                                            @else
                                                <li class="is-disable"><strong>---</strong></li>
                                            @endif
                                            </ul>
                                        @endif
                                </td>

                                <td width="25%" class="text-right amounts">
                                    @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                        <span class="is-delivered">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($petitions['products']['details']['amount'],$account['currency'], $petitions['cant']) }}</span>
                                    @else
                                        <span class="is-disable">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($petitions['products']['details']['amount'],$account['currency'], $petitions['cant']) }}</span>
                                    @endif
                                    {{--Extras--}}
                                    @if(count($petitions['products']['details']['extras']) > 0)
                                        <ul>
                                            @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                <li class="is-delivered" style="margin-right: 8%"><strong>---</strong></li>
                                            @else
                                                <li class="is-disable" style="margin-right: 8%"><strong>---</strong></li>
                                            @endif
                                        </ul>
                                        @foreach($petitions['products']['details']['extras'] as $extra)
                                            <ul>
                                                @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                    <li class="is-delivered">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($extra['price'][$account['currency']['iso']],$account['currency'], $petitions['cant']) }}</li>
                                                @else
                                                    <li class="is-disable">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($extra['price'][$account['currency']['iso']],$account['currency'], $petitions['cant']) }}</li>
                                                @endif
                                            </ul>
                                        @endforeach
                                    @endif

                                    {{--Characteristic--}}
                                    @if(count($petitions['products']['details']['characteristic']) > 0)
                                        @foreach($petitions['products']['details']['characteristic'] as $characteristic)
                                            <ul>
                                                @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                    <li class="is-delivered" style="margin-right: 8%"><strong>---</strong></li>
                                                @else
                                                    <li class="is-disable" style="margin-right: 8%"><strong>---</strong></li>
                                                @endif
                                            </ul>
                                            @foreach($characteristic['options'] as $options)
                                                <ul>
                                                    @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                        <li class="is-delivered">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($options['price'],$account['currency'], $petitions['cant']) }}</li>
                                                    @else
                                                        <li class="is-disable">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($options['price'],$account['currency'], $petitions['cant']) }}</li>
                                                    @endif
                                                </ul>
                                            @endforeach
                                        @endforeach
                                    @endif

                                        {{--Contours--}}
                                        @if(isset($petitions['products']['details']['contours']))
                                            @if(count($petitions['products']['details']['contours']) > 0)
                                                <ul>
                                                    @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                        <li class="is-delivered"><strong>---</strong></li>
                                                    @else
                                                        <li class="is-disable"><strong>---</strong></li>
                                                    @endif
                                                </ul>
                                                @foreach($petitions['products']['details']['contours'] as $contours)
                                                    <ul>
                                                        @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                            <li class="is-delivered"><strong>---</strong></li>
                                                        @else
                                                            <li class="is-disable"><strong>---</strong></li>
                                                        @endif
                                                    </ul>
                                                @endforeach
                                            @endif
                                        @endif

                                        {{--Replaceables--}}
                                        @if(isset($petitions['products']['details']['replaceables']))
                                            @if(count($petitions['products']['details']['replaceables']) > 0)
                                                <ul>
                                                    @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                        <li class="is-delivered"><strong>---</strong></li>
                                                    @else
                                                        <li class="is-disable"><strong>---</strong></li>
                                                    @endif
                                                </ul>
                                                @foreach($petitions['products']['details']['replaceables'] as $replaceables)
                                                    <ul>
                                                        @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                            <li class="is-delivered"><strong>---</strong></li>
                                                        @else
                                                            <li class="is-disable"><strong>---</strong></li>
                                                        @endif
                                                    </ul>
                                                @endforeach
                                            @endif
                                        @endif

                                        {{--Ingredients--}}
                                        @if(isset($petitions['products']['details']['ingredients']))
                                            @if(count($petitions['products']['details']['ingredients']) > 0)
                                                <ul>
                                                    @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                        <li class="is-delivered"><strong>---</strong></li>
                                                    @else
                                                        <li class="is-disable"><strong>---</strong></li>
                                                    @endif
                                                </ul>
                                                @foreach($petitions['products']['details']['ingredients'] as $ingredients)
                                                    <ul>
                                                        @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                            <li class="is-delivered"><strong>---</strong></li>
                                                        @else
                                                            <li class="is-disable"><strong>---</strong></li>
                                                        @endif
                                                    </ul>
                                                @endforeach
                                            @endif
                                        @endif

                                        @if(isset($petitions['products']['details']['observation']))
                                            <ul>
                                                @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                    <li class="is-delivered"><strong>---</strong></li>
                                                @else
                                                    <li class="is-disable"><strong>---</strong></li>
                                                @endif
                                            </ul>
                                            <ul>
                                                @if($petitions['products']['status_'] == \App\Petitions\Enums\PetitionsStatusEnum::$delivered)
                                                    <li class="is-delivered"><strong>---</strong></li>
                                                @else
                                                    <li class="is-disable"><strong>---</strong></li>
                                                @endif
                                            </ul>
                                        @endif
                                </td>
                            @endif
                        </tr>
                        </tbody>
                    @endforeach
                @else
                    <thead>
                    <tr>
                        <td width="100%" class="text-center empty">{{ __('No hay ordenes en esta cuenta') }}</td>
                    </tr>
                    </thead>
                @endif
            </table>
            @if(count($account['petitions']) > 0)
                <table class="table table-totals" width="100%">
                    <tbody>
                    <tr>
                        <td width="60%" class="text-left">{{ __('SubTotal') }}</td>
                        <td width="30%" class="text-right" style="padding-right: 5px; font-weight: bold;">{{ $account['currency']['symbol'] }}{{ $account['sub_total'] }}</td>
                    </tr>

                    @if(count($account['details']) > 0)
                        @if(count($account['details']['percentage_special']) > 0)
                            @foreach($account['details']['percentage_special'] as $percentage_special)
                                <tr>
                                    <td width="60%" class="text-left">{{ $percentage_special['name'] }} ({{ $percentage_special['operator'] }}{{ $percentage_special['percentage'] }}%)</td>
                                    <td width="30%" class="text-right" style="padding-right: 5px; font-weight: bold;">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($percentage_special['amount'],$account['currency']) }}</td>
                                </tr>
                            @endforeach
                        @endif

                        @if(count($account['details']['tip']) > 0)
                            <tr>
                                <td width="60%" class="text-left">{{ __('Propina') }}({{ $account['details']['tip_percentage'] }}%)
                                </td>
                                <td width="30%"
                                    class="text-right" style="padding-right: 5px; font-weight: bold;">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($account['details']['tip'],$account['currency']) }}</td>
                            </tr>
                        @endif
                        @if(count($account['details']['tax']) > 0)
                            <tr>
                                <td width="60%" class="text-left">{{ $account['details']['abbrev_tax'] }}({{ $account['details']['tax'] }}%)
                                </td>
                                <td width="30%"
                                    class="text-right" style="padding-right: 5px; font-weight: bold;">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($account['details']['tax_amount'],$account['currency']) }}</td>
                            </tr>
                        @endif

                        @if(count($account['details']['amount_total']) > 0)
                            <tr>
                                <td width="60%" class="text-left total">{{ __('Total') }}
                                </td>
                                <td width="30%" class="text-right total">{{ $account['currency']['symbol'] }}{{ \App\Helper::formatCurrency($account['details']['amount_total'],$account['currency']) }}</td>
                            </tr>
                        @endif
                    @endif
                    </tbody>
                </table>
            @endif
        </div>
    @endforeach
@else
    <div class="accounts">
        <table class="table table-totals" width="100%">
            <thead>
            <tr>
                <td width="100%" class="text-center empty">{{ __('No hay ') }} {{ $title }}</td>
            </tr>
            </thead>
        </table>
    </div>
@endif

<br />
<!-- Custom HTML footer -->
<div class="footer">
    www.TotusFood.com
</div>









<?php

// SETTINGS PRODUCTION
if (env('API_REST') === 'https://api.totusfood.com/printer/') {
    return [
        'HTTP' => 'https://',
        'API_REST' => 'https://api.totusfood.com/printer/',
        'URL_BACK' => 'https://back.totusfood.com/printer/',
        'URL_BACK_GENERAL' => 'https://back.totusfood.com',
        'DOMAIN' => 'totusfood.com',
        'URL_BACK_STORE' => 'https://back.totusfood.com/update/app/',
        'SERVER_SOCKET' => 'https://socket.totusfood.com',
        'SERVER_SOCKET_PORT' => 3000,
        'RETURN_IMAGE' => 'https://totusfoodpic.s3.amazonaws.com/upload/img/restaurants/',
    ];
}
// SETTINGS DEVELOP
if (env('API_REST') === 'https://api.totusfood.net/printer/') {
    return [
        'HTTP' => 'https://',
        'API_REST' => 'https://api.totusfood.net/printer/',
        'URL_BACK' => 'https://back.totusfood.net/printer/',
        'URL_BACK_GENERAL' => 'https://back.totusfood.net',
        'DOMAIN' => 'totusfood.net',
        'URL_BACK_STORE' => 'https://back.totusfood.net/update/app/',
        'SERVER_SOCKET' => 'https://socket.totusfood.com',
        'SERVER_SOCKET_PORT' => 3000,
        'RETURN_IMAGE' => "https://totusfoodpic.s3.amazonaws.com/upload/img/restaurants/",
    ];
}

// SETTINGS LOCAL

if (env('API_REST') === 'http://api.totusfood.co/printer/') {
    return [
        'HTTP' => 'http://',
        'API_REST' => 'http://api.totusfood.co/printer/',
        'URL_BACK' => 'http://back.totusfood.co/printer/',
        'URL_BACK_GENERAL' => 'http://back.totusfood.co',
        'DOMAIN' => 'totusfood.co',
        'URL_BACK_STORE' => 'http://back.totusfood.co/update/app/',
        'SERVER_SOCKET' => 'http://totusfood.co',
        'SERVER_SOCKET_PORT' => 3000,
        'RETURN_IMAGE' => 'http://back.totusfood.co/upload/img/restaurants/',
    ];
}


<?php

namespace App\Http\Controllers;

use App\Helper;
use App\Printer\Model\Printers;
use App\Printer\PrinterESCPOS;
use App\Printer\Repo\PrinterRepo;
use App\Restaurants\Model\Restaurants;
use App\Restaurants\Repo\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;


class PrintController extends Controller
{
    private $printerESCPOS;
    private $restaurantModel;
    private $printers;
    function __construct(PrinterESCPOS $printerESCPOS, Restaurant $restaurantModel, PrinterRepo $printers)
    {
        $this -> printerESCPOS   = $printerESCPOS;
        $this -> restaurantModel = $restaurantModel;
        $this -> printers        = $printers;
    }


    public function index()
    {

        $restaurant = [];
        $restaurants = Restaurants::where('remember', true)->where('current', true)->get();
        $remember = count($restaurants) > 0 ? true : false;
        $restaurantName = $remember == true ? $restaurants[0]->name : '';
        $restaurantID = $remember == true ? $restaurants[0]->id : '';
        $restaurant['id'] = $restaurantID;
        $restaurant['name'] = $restaurantName;
        $restaurant = Helper::array_object($restaurant);
        $settings = Helper::array_object(Config::get('settings'));
        $version = File::get(base_path().'/version.txt');
        if (session()->get('restaurant.id')) {
            $restaurant = $this->restaurantModel->all(session()->get('restaurant.id'));
            return view('dashboard', ['version' => $version, 'settings' => $settings, 'restaurant' => $restaurant]);
        } else {
            return view('login', ['version' => $version, 'settings' => $settings, 'remember' => $remember, 'restaurant' => $restaurant]);
        }
    }

    public function store(Request $request)
    {
        $printers = [];
        $printers = $request->get('data');
        if ($request->has('data') && count($request->get('data')) > 0) {
            foreach ($printers as $printer) {
                $exist = Printers::where('id', $printer['id'])->where('restaurant_id', session()->get('restaurant.id'))->get();
                if (count($exist) > 0) {
                    $this->printers->updatePrinters($printer);
                } else {
                    $printers           = new Printers;
                    $printers->id       = $printer['id'];
                    $printers->title    = $printer['title'];
                    $printers->type     = $printer['type'];
                    $printers->source   = $printer['source'];
                    $printers->host     = $printer['host'];
                    $printers->port     = $printer['port'];
                    $printers->status   = $printer['status'];
                    $printers->restaurant_id = session()->get('restaurant.id');
                    $printers->predetermined = false;
                    $printers->save();
                }
            }
        }
    }

    public function dashboard(Request $request)
    {
        $data = $request->get('response');
        $restaurant = $this->restaurantModel->all($data['id']);

        if(is_null($restaurant)) {
            $this->restaurantModel->store($data);
        } else {
            $remember = isset($data['remember']) ? $data['remember'] : $restaurant->remember;
            $this->restaurantModel->refreshToken($data['id'], $data['token'], $remember);
        }

        $restaurant = Restaurants::find($data['id']);

        $request->session()->put('restaurant.id', $data['id']);
        $request->session()->put('restaurant.token', $data['token']);
        $request->session()->put('restaurant.purchased', $data['purchased']);
        $request->session()->put('restaurant.name', $restaurant->name);
        $data = [
            'status' => 'OK',
            'title' => __('Servicio conectado') . '.',
            'message' => __('Has sido conectado al servicio de impresiones Totus Food Printer correctamente') . ".",
            'state' => 'true'
        ];
        SocketIO::emit('TotusFood::server ' . session()->get('restaurant.token') . ' printer-connected', ['data' => $data]);
        return [
            'url' => route('index'),
            'status' => 'OK',
            'title' => __('Servicio conectado') . '.',
            'message' => __('Has sido conectado al servicio de impresiones Totus Food Printer correctamente') . ".",
            'state' => 'true',
            'logged' => 'true'
        ];
    }

    public function dashboardRoot(Request $request)
        {
            $data = $request->get('response');
            $request->session()->put('restaurant.id', 'root');
            $request->session()->put('restaurant.token', 'TotusFood');
            $request->session()->put('restaurant.name', 'TotusFood');
            $data = [
                'status' => 'OK',
                'title' => __('Servicio conectado') . '.',
                'message' => __('Has sido conectado al servicio de impresiones Totus Food Printer correctamente') . ".",
                'state' => 'true'
            ];
            SocketIO::emit('TotusFood::server TotusFood printer-connected-root', ['data' => $data]);
            return [
                'url' => route('index'),
                'status' => 'OK',
                'title' => __('Servicio conectado') . '.',
                'message' => __('Has sido conectado al servicio de impresiones Totus Food Printer correctamente') . ".",
                'state' => 'true',
                'logged' => 'true'
            ];
        }

    public function printer(Request $request)
    {
        $received = $request->get('data');
        $source   = $received['data']['printer']['source'];
        $type     = $received['data']['printer']['type'];
        $host     = $received['data']['printer']['host'];
        $port     = $received['data']['printer']['port'];

        $printers = new Printers;
        $printer = $printers->where('title', $received['data']['printer']['title'])->where('restaurant_id', session()->get('restaurant.id'))->where('predetermined', true)->get();
        $predetermined = count($printer) > 0 ? true : false;

        if($predetermined) {
            $response = $this-> printerESCPOS->impression($received, $type, strtoupper($source), $host, $port, $received['data']['typePrint']);
            SocketIO::emit('TotusFood::server ' . session()->get('restaurant.token') . ' printer-response', ['data' => $response]);
        } else {
            $response = null;
        }
        return $response;
    }

    public function printerTest(Request $request)
    {
        try {
            $print = $request->get('data');
            $response = $this->printerESCPOS->TestPrinter($print['data'], $print['data']['type'], strtoupper($print['data']['source']), $print['data']['host'], $print['data']['port'], $print['data']['paper']);
            SocketIO::emit('TotusFood::server ' . session()->get('restaurant.token') . ' printer-response', ['data' => $response]);
        } catch (\Exception $exception) {
            $response = (object)[
                'error' => true,
                'message' => $exception->getMessage()
            ];
        }
        return $response;
    }

    public function printerTestLocal(Request $request)
    {
        try {
            $print = $request->get('data');
            $response = $this->printerESCPOS->TestPrinter($print, $print['type'], strtoupper($print['source']), $print['host'], $print['port'], $print['paper']);
        } catch (\Exception $exception) {
            $response = (object)[
                'error' => true,
                'message' => $exception->getMessage()
            ];
        }
        return $response;
    }

    public function testConnection(Request $request)
    {
        try {
            $print = $request->get('data');
            $response = $this->printerESCPOS->TestPrinterConnection($print, $print['type'], strtoupper($print['source']), $print['host'], $print['port'], $print['paper']);
        } catch (\Exception $exception) {
            $response = (object)[
                'error' => true,
                'message' => $exception->getMessage()
            ];
        }
        return $response;
    }

    public function printerRoot(Request $request)
    {
        try {
            $print   = $request->get('data');
            $printer = Helper::array_object($print);
            $response = $this->printerESCPOS->PrinterRoot($printer->data, $printer->data->printer->type, $printer->data->printer->source, $printer->data->printer->host, $printer->data->printer->port, $printer->data->printer->paper);
        } catch (\Exception $exception) {
            $response = (object)[
                'error' => true,
                'message' => $exception->getMessage()
            ];
        }
        return $response;
    }

    public function logout(Request $request)
    {
        $this->restaurantModel->logoutRestaurant(session()->get('restaurant.id'));
        $request->session()->forget('restaurant.id');
        $request->session()->forget('restaurant.name');
        $request->session()->forget('restaurant.token');
        $data = [
            'status' => 'OK',
            'title' => __('Servicio desconectado') . '.',
            'message' => __('Has sido desconectado al servicio de impresiones Totus Food Printer correctamente') . ".",
            'state' => 'false',
            'logout' => 'true'
        ];
        SocketIO::emit('TotusFood::server ' . session()->get('restaurant.token') . ' printer-connected', ['data' => $data]);
        return $data;
    }

    public function logOutRemember(Request $request)
    {
        $this->restaurantModel->logoutRestaurant($request->get('id'));
        $request->session()->forget('restaurant.id');
        $request->session()->forget('restaurant.name');
        $request->session()->forget('restaurant.token');
        $response = [
            'status' => 200
        ];
        return $response;
    }

    public function logoutRoot(Request $request)
    {
        $request->session()->forget('restaurant.id');
        $request->session()->forget('restaurant.name');
        $data = [
            'status' => 'OK',
            'title' => __('Servicio desconectado') . '.',
            'message' => __('Has sido desconectado al servicio de impresiones Totus Food Printer correctamente') . ".",
            'state' => 'false',
            'logout' => 'true'
        ];
        SocketIO::emit('TotusFood::server TotusFood printer-connected-root', ['data' => $data]);
        return redirect('/');
    }

    public function pong()
    {
        SocketIO::emit('TotusFood::server ' . session()->get('restaurant.token') . ' printer-ping-pong', ['data' => 'PONG']);
    }

    public function versionCurrent()
    {
        $version = File::get(base_path().'/version.txt');
        return $version;
    }

    public function setPrinter(Request $request)
    {
        $data = $request->get('data');
    }

    public function goToActionWeb(Request $request)
    {
       $url = $request->get('url');
        system('start ' . $url);
    }

    public function all()
    {
        $printers = Printers::where('restaurant_id', session()->get('restaurant.id'))->get();
        return $printers;
    }

    public function destroy(Request $request)
    {
        $printer = Printers::find($request->get('id'));
        $printer->delete();
    }

    public function trash(Request $request)
    {
        $printers = [];
        $printers = $request->get('data');
        if ($request->has('data') && count($request->get('data')) > 0) {
            foreach ($printers as $printer) {
                $exist = Printers::where('id', $printer['id'])->where('restaurant_id', session()->get('restaurant.id'))->get();
                if (count($exist) == 0)
                    continue;
                $printer = Printers::find($printer['id']);
                $printer->delete();
            }
        }
    }


    public function updatePredetermined(Request $request) {
        $id = $request->get('id');
        $predetermined = $request->get('predetermined');
        $printers = new Printers();
        $printer = Printers::find($id);
        $printers->where('id', $id)->where('restaurant_id', session()->get('restaurant.id'))->update(['predetermined' => $predetermined]);
    }

    public function havePredetermined()
    {
        $printers = new Printers();
        $printer = $printers->where('restaurant_id', session()->get('restaurant.id'))->where('predetermined', true)->get();
        $predetermined = count($printer) > 0 ? true : false;

        $response = [
            "status" => 200,
            "predetermined" => $predetermined,
        ];
        return $response;
    }


}



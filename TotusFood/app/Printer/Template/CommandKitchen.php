<?php

namespace App\Printer\Template;

use App\Helper;
use Mike42\Escpos\Printer;

class CommandKitchen
{

    public $printer;
    public $char_per_line = 0;
    public $currency_symbol = "";

    public function printer($contents, $connector)
    {
        try {
            $printer = new Printer($connector);
            $head = Helper::array_object($contents['data']['head']);
            $products = $contents['data']['products'];
            $this->char_per_line = $contents['data']['paper'];
            $textDate = Helper::validText(__("Fecha"));
            $textHour = Helper::validText(__("Hora"));
            $order = __("Orden");
            $textProduct = __("Productos");
            $textCant = __("Cantidad");
            $table = __('Mesa');
            $area = __('Area');
            $waiter = __('Mesero');
            $client = __('Cliente');
            $TextIngredients = __('Quitar ingredientes');
            $TextReplaceable = __('Reemplazar');
            $TextContour = __('Contorno');
            $TextExtras = __('Extras');
            $TextObservation = __('Observacion');
            $TextPortion = __('Porcion');
            $TextByReplaceable = __('por');
            $Credits = __('www.TotusFood.com');
            $date = $head->dateTime->date;
            $hour =$head->dateTime->hour;

            $printer->setTextSize(1, 2);
            $printer->text(Helper::centerText($head->title, $this->char_per_line));
            $printer->setTextSize(1, 1);
            $printer->feed(1);
            $printer->selectPrintMode();
            if (! empty($head->mode)) {
                $printer->setJustification(Printer::JUSTIFY_CENTER);
                $printer->text("{$head->mode} \n");
                $printer->setJustification();
            }
            $printer->text("{$order}: #{$head->order} \n");
            if (! empty($head->table)) {
				$printer->text(Helper::validText("{$area}: {$head->area} \n"));
                $printer->text(Helper::validText("{$table}: {$head->table} \n"));
            }
            if (! empty($head->waiter)) {
                $printer->text(Helper::validText("{$waiter}: {$head->waiter} \n"));
            }
            $printer->text(Helper::validText("{$client}: {$head->username} \n"));
            $printer->text(Helper::validText($this->printLine("{$textDate}:{$date}^{$textHour}:{$hour}") . "\n"));
            $printer->text($this->drawLine());
            $printer->selectPrintMode(Printer::MODE_EMPHASIZED);
            $printer->text("{$textCant}(x)  {$textProduct} \n");
            $printer->selectPrintMode(Printer::JUSTIFY_LEFT);
            $printer->text($this->drawLine());
            if (isset($products['petition'])) {
                $printer->text(Helper::validText("- {$products['cant']}(x) {$products['title']} \n"));
                $printer->selectPrintMode(Printer::UNDERLINE_SINGLE);
                $portion = strtolower($products['portion']);
                if(isset($portion)) {
                    if($portion != 'unica'){
                        $printer->text(Helper::validText(" * {$TextPortion}: {$products['portion']} - {$products['cant_details']}\n"));
                    }
                }
                $printer->selectPrintMode();
                if (isset($products['ingredients'])) {
                    if (count($products['ingredients']) > 0) {
                        $printer->selectPrintMode(Printer::UNDERLINE_SINGLE);
                        $printer->text(Helper::validText(" * {$TextIngredients}: "));
                        foreach ($products['ingredients'] as $index => $items) {
                            if ($items === end($products['ingredients'])) {
                                $printer->text(Helper::validText("{$items['title']}. \n"));
                            } else {
                                $printer->text(Helper::validText("{$items['title']}, "));
                            }
                        }
                        $printer->selectPrintMode();
                        $products['ingredients'] = [];
                    }
                }
                if (isset($products['replaceables'])) {
                    if (count($products['replaceables']) > 0) {
                        $printer->selectPrintMode(Printer::UNDERLINE_SINGLE);
                        $printer->text(Helper::validText(" * {$TextReplaceable}: "));
                        foreach ($products['replaceables'] as $index => $items) {
                            if ($items === end($products['replaceables'])) {
                                $printer->text(Helper::validText("{$items['titleOld']} {$TextByReplaceable} {$items['titleNew']}. \n"));
                            } else {
                                $printer->text(Helper::validText("{$items['titleOld']} {$TextByReplaceable} {$items['titleNew']}, "));
                            }
                        }
                        $printer->selectPrintMode();
                        $products['replaceables'] = [];
                    }
                }

                if (isset($products['extras'])) {
                    if (count($products['extras']) > 0) {
                        $printer->selectPrintMode(Printer::UNDERLINE_SINGLE);
                        $printer->text(Helper::validText(" * {$TextExtras}: "));
                        foreach ($products['extras'] as $index => $items) {
                            if ($items === end($products['extras'])) {
                                $printer->text(Helper::validText("{$items['title']}. \n"));
                            } else {
                                $printer->text(Helper::validText("{$items['title']}, "));
                            }
                        }
                        $printer->selectPrintMode();
                        $products['extras'] = [];
                    }
                }
                if (isset($products['contours'])) {
                    if (count($products['contours']) > 0) {
                        $printer->selectPrintMode(Printer::UNDERLINE_SINGLE);
                        $printer->text(Helper::validText(" * {$TextContour}: "));
                        foreach ($products['contours'] as $index => $items) {
                            if ($items === end($products['contours'])) {
                                $printer->text(Helper::validText("{$items['title']}. \n"));
                            } else {
                                $printer->text(Helper::validText("{$items['title']}, "));
                            }
                        }
                        $printer->selectPrintMode();
                        $products['contours'] = [];
                    }
                }

                if (isset($products['characteristic'])) {
                    if (count($products['characteristic']) > 0) {
                        $printer->selectPrintMode(Printer::UNDERLINE_SINGLE);
                        foreach ($products['characteristic'] as $index => $characteristic) {
                            $printer->text(Helper::validText(" * {$characteristic['title_characteristic']}: "));
                            foreach ($characteristic['options'] as $option) {
                                if ($option === end($characteristic['options'])) {
                                    $printer->text(Helper::validText("{$option['name']}. \n"));
                                } else {
                                    $printer->text(Helper::validText("{$option['name']}, "));
                                }
                            }
                        }
                        $printer->selectPrintMode();
                        $products['characteristic'] = [];
                    }
                }

                if (isset($products['observation']) and !empty($products['observation'])) {
                    $printer->selectPrintMode(Printer::UNDERLINE_SINGLE);
                    $printer->text(Helper::validText("- {$TextObservation}: "));
                    $printer->text(Helper::validText("{$products['observation']}."));
                    $printer->selectPrintMode();
                }

            } else { //TODO ORDER
                foreach ($products as $product__) {
                    foreach ($product__ as $product) {
                    // TODO Multiples Type Products
                    if ($this->is_array_bidimencional($product)) {
                        $printer->text(Helper::validText("- {$product['cant']}(x) {$product['title']} \n"));
                        $printer->selectPrintMode(Printer::UNDERLINE_SINGLE);
                        $portion = strtolower($product['details']['portion']);
                        if(isset($portion)) {
                            if($portion != 'unica'){
                                $printer->text(Helper::validText(" * {$TextPortion}: {$product['details']['portion']} - {$product['details']['cant_details']}\n"));
                            }
                        }
                        $printer->selectPrintMode();
                        // TODO Ingredients
                        if (isset($product['details']['ingredients'])) {
                            if (count($product['details']['ingredients']) > 0) {
                                $printer->selectPrintMode(Printer::UNDERLINE_SINGLE);
                                $printer->text(Helper::validText(" * {$TextIngredients}: "));
                                foreach ($product['details']['ingredients'] as $index => $items) {
                                    if ($items === end($product['details']['ingredients'])) {
                                        $printer->text(Helper::validText("{$items['title']}. \n"));
                                    } else {
                                        $printer->text(Helper::validText("{$items['title']}, "));
                                    }
                                }
                                $printer->selectPrintMode();
                                $product['details']['ingredients'] = [];
                            }
                        }
                        // TODO Replaceables
                        if (isset($product['details']['replaceables'])) {
                            if (count($product['details']['replaceables']) > 0) {
                                $printer->selectPrintMode(Printer::UNDERLINE_SINGLE);
                                $printer->text(" * {$TextReplaceable}: ");
                                foreach ($product['details']['replaceables'] as $index => $items) {
                                    if ($items === end($product['details']['replaceables'])) {
                                        $printer->text(Helper::validText("{$items['titleOld']} {$TextByReplaceable} {$items['titleNew']}. \n"));
                                    } else {
                                        $printer->text(Helper::validText("{$items['titleOld']} {$TextByReplaceable} {$items['titleNew']}, "));
                                    }
                                }
                                $printer->selectPrintMode();
                            }
                        }
                        // TODO Extras
                        if (isset($product['details']['extras'])) {
                            if (count($product['details']['extras']) > 0) {
                                $printer->selectPrintMode(Printer::UNDERLINE_SINGLE);
                                $printer->text(Helper::validText(" * {$TextExtras}: "));
                                foreach ($product['details']['extras'] as $index => $items) {
                                    if ($items === end($product['details']['extras'])) {
                                        $printer->text(Helper::validText("{$items['title']}. \n"));
                                    } else {
                                        $printer->text(Helper::validText("{$items['title']}, "));
                                    }
                                }
                                $printer->selectPrintMode();
                                $product['details']['extras'] = [];
                            }
                        }
                        // TODO Characteristics
                        if (isset($product['details']['characteristic'])) {
                            if (count($product['details']['characteristic']) > 0) {
                                $printer->selectPrintMode(Printer::UNDERLINE_SINGLE);
                                foreach ($product['details']['characteristic'] as $characteristic) {
                                    $printer->text(Helper::validText(" * {$characteristic['title_characteristic']}: "));
                                    foreach ($characteristic['options'] as $option) {
                                        if ($option === end($characteristic['options'])) {
                                            $printer->text(Helper::validText("{$option['name']}. \n"));
                                        } else {
                                            $printer->text(Helper::validText("{$option['name']}, "));
                                        }
                                    }
                                }
                                $printer->selectPrintMode();
                                $product['details']['characteristic'] = [];
                            }
                        }

                        // TODO
                        if (isset($product['details']['contours'])) {
                            if (count($product['details']['contours']) > 0) {
                                $printer->selectPrintMode(Printer::UNDERLINE_SINGLE);
                                $printer->text(Helper::validText(" * {$TextContour}: "));
                                foreach ($product['details']['contours'] as $index => $items) {
                                    if ($items === end($product['details']['contours'])) {
                                        $printer->text(Helper::validText("{$items['title']}. \n"));
                                    } else {
                                        $printer->text(Helper::validText("{$items['title']}, "));
                                    }
                                }
                                $printer->selectPrintMode();
                                $product['details']['contours'] = [];
                            }
                        }

                        // TODO Observations
                        if (isset($product['observation']) and !empty($product['observation'])) {
                            $printer->selectPrintMode(Printer::UNDERLINE_SINGLE);
                            $printer->text(Helper::validText("- {$TextObservation}: "));
                            $printer->text(Helper::validText("{$product['observation']}."));
                            $printer->selectPrintMode();
                        }

                    } // TODO Only Type Product
                    else {
                        if (isset($product['name'])) {
                            $printer->text(Helper::validText("* " . $product['name'] . "\n"));
                        }
                        if (isset($product['portion'])) {
                            $printer->text(Helper::validText($TextPortion . $product['portion'] . "\n"));
                        }
                        // TODO Ingredients
                        if (isset($product['ingredients'])) {
                            if (count($product['ingredients']) > 0) {
                                $printer->text(Helper::validText(" * {$TextIngredients}: "));
                                foreach ($product['ingredients'] as $index => $items) {
                                    if ($items === end($product['ingredients'])) {
                                        $printer->text(Helper::validText("{$items['title']}. \n"));
                                    } else {
                                        $printer->text(Helper::validText("{$items['title']}, "));
                                    }
                                }
                            }
                        }
                        // TODO Replaceables
                        if (isset($product['replaceables'])) {
                            if (count($product['replaceables']) > 0) {
                                $printer->text(Helper::validText(" * {$TextReplaceable}: "));
                                foreach ($product['replaceables'] as $index => $items) {
                                    if ($items === end($product['replaceables'])) {
                                        $printer->text(Helper::validText("{$items['titleOld']} {$TextByReplaceable} {$items['titleNew']}. \n"));
                                    } else {
                                        $printer->text(Helper::validText("{$items['titleOld']} {$TextByReplaceable} {$items['titleNew']}, "));
                                    }
                                }
                                $product['replaceables'] = [];
                            }
                        }
                        // TODO Extras
                        if (isset($product['extras'])) {
                            if (count($product['extras']) > 0) {
                                $printer->text(Helper::validText(" * {$TextExtras}: "));
                                foreach ($product['extras'] as $index => $items) {
                                    if ($items === end($product['extras'])) {
                                        $printer->text(Helper::validText("{$items['title']}. \n"));
                                    } else {
                                        $printer->text(Helper::validText("{$items['title']}, "));
                                    }
                                }
                                $product['extras'] = [];
                            }
                        }
                        // TODO Contours
                        if (isset($product['contours'])) {
                            if (count($product['contours']) > 0) {
                                $printer->text(Helper::validText(" * {$TextContour}: "));
                                foreach ($product['contours'] as $index => $items) {
                                    if ($items === end($product['contours'])) {
                                        $printer->text(Helper::validText("{$items['title']}. \n"));
                                    } else {
                                        $printer->text(Helper::validText("{$items['title']}, "));
                                    }
                                }
                                $product['contours'] = [];
                            }
                        }

                        // TODO Characteristics
                        if (isset($product['characteristic'])) {
                            if (count($product['characteristic']) > 0) {
                                $printer->selectPrintMode(Printer::UNDERLINE_SINGLE);
                                foreach ($product['characteristic'] as $characteristic) {
                                    $printer->text(Helper::validText(" * {$characteristic['title_characteristic']}: "));
                                    foreach ($characteristic['options'] as $option) {
                                        if ($option === end($characteristic['options'])) {
                                            $printer->text(Helper::validText("{$option['name']}. \n"));
                                        } else {
                                            $printer->text(Helper::validText("{$option['name']}, "));
                                        }
                                    }
                                }
                                $printer->selectPrintMode();
                                $product['characteristic'] = [];
                            }
                        }

                        // TODO Observations
                        if (isset($product['observation']) and !empty($product['observation'])) {
                            $printer->selectPrintMode(Printer::UNDERLINE_SINGLE);
                            $printer->text(Helper::validText("- {$TextObservation}: "));
                            $printer->text(Helper::validText("{$product['observation']}."));
                            $printer->selectPrintMode();
                        }
                    }
                }
            }
        }

            $printer->feed();
            $printer->feed();
            $printer->feed();
            $printer->cut();
            $printer->close();
            $response = [
                'title' => __('¡Listo!'),
                'status' => 200,
                'message' => __("{$order}: #{$head->order}, impreso correctamente"),

            ];
        } catch (\Exception $exception) {
            $response = [
                'title' => __('¡Error!'),
                'status' => 500,
                'message' => __('No se puede conectar a una impresora, por favor verifica el estado, puertos de conexión e intente nuevamente') . "."
            ];
        }
        return $response;
    }

    private function is_array_bidimencional($array)
    {
        if (count($array) !== count($array, COUNT_RECURSIVE)) return true;
        else return false;
    }

    private function countRows($array)
    {
        $cant = 0;
        foreach ($array as $element) {
            $cant++;
        }
        return $cant;
    }


    function full_str_pad($input, $pad_length, $pad_string = '', $pad_type = 0)
    {
        $str = '';
        $length = $pad_length - strlen($input);
        if ($length > 0) { // str_repeat doesn't like negatives
            if ($pad_type == STR_PAD_RIGHT) { // STR_PAD_RIGHT == 1
                $str = $input . str_repeat($pad_string, $length);
            } elseif ($pad_type == STR_PAD_BOTH) { // STR_PAD_BOTH == 2
                $str = str_repeat($pad_string, floor($length / 2));
                $str .= $input;
                $str .= str_repeat($pad_string, ceil($length / 2));
            } else { // defaults to STR_PAD_LEFT == 0
                $str = str_repeat($pad_string, $length) . $input;
            }
        } else { // if $length is negative or zero we don't need to do anything
            $str = $input;
        }
        return $str;
    }

    function drawLine()
    {
        $new = '';
        for ($i = 1; $i < $this->char_per_line; $i++) {
            $new .= '-';
        }
        return $new . "\n";

    }

    function printLine($str, $size = NULL, $sep = "^", $space = NULL)
    {
        if (!$size) {
            $size = $this->char_per_line;
        }
        $size = $space ? $space : $size;
        $length = strlen($str);
        list($first, $second) = explode("^", $str, 2);
        $line = $first . ($sep == "^" ? $sep : '');
        for ($i = 1; $i < ($size - $length); $i++) {
            $line .= ' ';
        }
        $line .= ($sep != "^" ? $sep : '') . $second;
        $line = str_replace("^", " ", $line);
        return $line;
    }

}
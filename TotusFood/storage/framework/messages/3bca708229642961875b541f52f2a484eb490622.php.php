<!doctype html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>"/>
    <title>Printer Desktop TotusFood</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css"/>
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet" type="text/css"/>

    <link href="<?php echo e(asset('assets/toastr/toastr.min.css')); ?>" rel="stylesheet"/>
    <!-- Favicon and touch icons -->
    

    <!-- Bootstrap -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet"/>
    <link href="<?php echo e(asset('assets/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet"/>
    <link href="<?php echo e(asset('assets/toastr/toastr.min.css')); ?>" rel="stylesheet"/>
    <link href="<?php echo e(asset('css/styleBD.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/font-awesome/css/font-awesome.css')); ?>" rel="stylesheet"/>
    <link href="<?php echo e(asset('assets/sweetalert2/sweetalert2.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/sweetalert2/sweetalert_custom.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/parsley/parsley.css')); ?>" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
    <script src="<?php echo e(asset('js/socket.io.js')); ?>"></script>
    <script src="<?php echo e(asset('js/jquery.js')); ?>"></script>
    <!-- jQuery rest -->
    <script src="<?php echo e(asset('js/jquery.rest.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/toastr/toastr.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/toastrPersonalized.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/parsley/parsley.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/sweetalert2/sweetalert2.js')); ?>"></script>
    <script src="<?php echo e(asset('js/printer.js')); ?>"></script>
    <script src="<?php echo e(asset('js/env.js')); ?>"></script>
</head>
<body>
    <?php if(session()->get('restaurant.id')): ?>
        <div class="navbar-wrapper">
            <div class="container-fluid">
                <nav class="navbar navbar-fixed-top">
                    <div class="container">
                        <div class="navbar-header">
                            <img src="<?php echo e(asset('img/logo.png')); ?>" alt="" class="logo-bar"> <span class="restaurant-name"> <?php echo e(ucwords(session()->get('restaurant.name'))); ?></span>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                
                                
                                
                            </ul>
                            <ul class="nav navbar-nav pull-right">
                                <li class="active"><a id="logout" data-url="<?php echo e(route('logout')); ?>" ><?php echo e(__('Cerrar sesión')); ?></a> </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <?php endif; ?>
    <?php echo $__env->yieldContent('content'); ?>
    <input type="hidden" id="api" value="<?php echo e(env('API_REST')); ?>"/>
    <?php if(session()->get('restaurant.id')): ?>
    <footer class="footer">
        <div class="pull-right hidden-xs"><?php echo e(env('VERSION_APP')); ?></div>
        <strong>Copyright &copy; 2016-<?php echo e(date('Y')); ?> <a href="">TotusFood</a>.</strong> <?php echo e(__('Todos Los derechos reservados')); ?>.
    </footer>
    <?php endif; ?>
</body>
    <script>
        $(function() {
            var settings = {
                titleLogout: "<?php echo e(__('¿Desea cerrar sesión?')); ?>",
                message: "<?php echo e(__('Debe asegurarse de no tener impresiones en cola, al cerrar este programa todas las impresiones son canceladas')); ?>.",
                accept:  "<?php echo e(__('Aceptar')); ?>",
                cancel:  "<?php echo e(__('Cancelar')); ?>",
                messasage_logout: "<?php echo e(__('¡Ha, cerrado sesión con éxitos!')); ?>",
                buttons: {
                    confirmText: '<?php echo e(__('¡Si, cerrar sesión!')); ?>',
                    cancelText: '<?php echo e(__('¡No, cancelar!')); ?>'
                }
            };
            Printer.init(settings);
        });
    </script>
    <?php echo $__env->yieldContent('scripts'); ?>
</html>
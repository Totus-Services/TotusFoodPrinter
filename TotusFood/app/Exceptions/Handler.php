<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception) {
        if($exception->getMessage() != "" && $exception->getMessage() != "Unauthenticated.") {
            if (env('APP_ENV') === 'production') {
                $client   = new Client();
                $message  = "<b>Project:</b> " . "BackOffice  \n" . "<b>File:</b> " . $exception->getFile() . "\n<b>Line:</b> " . $exception->getLine() . "\n<b>Error:</b> " . $exception->getMessage();
                $response = $client->request('POST', 'https://api.telegram.org/bot606713568:AAHt63Q2tUunW8p0UQ-AAErh7rzLc1O0L-8/sendMessage', [
                    'form_params' => [
                        'chat_id'    => -1001394253543,
                        'text'       => $message,
                        'parse_mode' => 'HTML',
                    ],
                ]);

            }
        }

        parent::report($exception);
    }


    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if($exception->getMessage() != "" && $exception->getMessage() != "Unauthenticated.") {
            if (env('APP_ENV') === 'production') {
                $client   = new Client();
                $message  = "<b>Project:</b> " . "BackOffice  \n" . "<b>File:</b> " . $exception->getFile() . "\n<b>Line:</b> " . $exception->getLine() . "\n<b>Error:</b> " . $exception->getMessage();
                $response = $client->request('POST', 'https://api.telegram.org/bot606713568:AAHt63Q2tUunW8p0UQ-AAErh7rzLc1O0L-8/sendMessage', [
                    'form_params' => [
                        'chat_id'    => -1001394253543,
                        'text'       => $message,
                        'parse_mode' => 'HTML',
                    ],
                ]);

            }
        }

        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('login');
    }
}

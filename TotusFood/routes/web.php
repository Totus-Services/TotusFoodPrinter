<?php

Route::group(['middleware' => [
    'cors',
], 'prefix' => '/'], function () {


    Route::post('dashboard', [
        'as' => 'index',
        'uses' => 'PrintController@dashboard',
    ]);


    Route::get('/', [
        'as' => 'index',
        'uses' => 'PrintController@index',
    ]);


//Route::get('/login/auth', [
//    'as' => 'login.auth',
//    'uses' => 'PrintController@printerTest',
//]);

    Route::post('dashboard/root', [
        'as' => 'index',
        'uses' => 'PrintController@dashboardRoot',
    ]);


    Route::post('/login/auth', function () {
        return view('login');
    });


    Route::post('/printer', [
        'as' => 'printer',
        'uses' => 'PrintController@printer',
    ]);

    Route::post('/printer/invoice/root', [
        'as' => 'printer.invoice.root',
        'uses' => 'PrintController@printerRoot',
    ]);

    Route::post('/printer/test', [
        'as' => 'printer.test',
        'uses' => 'PrintController@printerTest',
    ]);

    Route::post('/printer/pong', [
        'as' => 'printer.pong',
        'uses' => 'PrintController@pong',
    ]);

    Route::post('/logout', [
        'as' => 'logout',
        'uses' => 'PrintController@logout',
    ]);

    Route::get('/logout/root', [
        'as' => 'logout.root',
        'uses' => 'PrintController@logoutRoot',
    ]);

    Route::post('/logout/remember', [
        'as' => 'logout.remember',
        'uses' => 'PrintController@logOutRemember',
    ]);

    Route::post('/update', [
        'as' => 'update',
        'uses' => 'TotusUpdaterController@update',
    ]);

    Route::post('/set/printer', [
        'as' => 'set.printer',
        'uses' => 'TotusUpdaterController@setPrinter',
    ]);

    Route::post('/printer/test/local', [
        'as' => 'printer.test.local',
        'uses' => 'PrintController@printerTestLocal',
    ]);

    Route::post('/printer/test/connection', [
        'as' => 'printer.test.connection',
        'uses' => 'PrintController@testConnection',
    ]);

    Route::post('/printer/gotoweb', [
        'as' => 'printer.gotoweb',
        'uses' => 'PrintController@goToActionWeb',
    ]);


    Route::get('/printer/version', [
        'as' => 'printer.version',
        'uses' => 'PrintController@versionCurrent',
    ]);

    Route::get('/printer/all', [
        'as' => 'printer.all',
        'uses' => 'PrintController@all',
    ]);

    Route::post('/printer/store', [
        'as' => 'printer.store',
        'uses' => 'PrintController@store',
    ]);


    Route::post('/printer/trash', [
        'as' => 'printer.trash',
        'uses' => 'PrintController@trash',
    ]);

    Route::post('/printer/delete', [
        'as'   => 'printer.delete',
        'uses' => 'PrintController@destroy',
    ]);

    Route::get('/printer/predetermined', [
        'as'   => 'printer.predetermined',
        'uses' => 'PrintController@havePredetermined',
    ]);


});
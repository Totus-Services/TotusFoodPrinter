<?php

namespace App\Petitions\Model;

use Illuminate\Database\Eloquent\Model;

class Petitions extends Model
{
    protected $table = 'petitions';

    protected $fillable = ['id', 'petition_id', 'order_id', 'type', 'product', 'status', 'amount', 'details', 'account_id', 'restaurant_id', 'created_at', 'updated_at'];


}

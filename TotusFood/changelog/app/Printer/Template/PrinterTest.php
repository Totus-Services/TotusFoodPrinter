<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/3/2018
 * Time: 10:40
 */

namespace App\Printer\Template;


use App\Helper;
use Mike42\Escpos\Printer;

class PrinterTest
{

    public $printer;
    public $char_per_line = 0;
    public $currency_symbol = "";
    public function printer($content, $connector, $paper)
    {
        try {
        $this->printer = new Printer($connector);
        $this->char_per_line = $paper;
        $message = __('Prueba de impresión ');
        $title   = session()->get('restaurant.name');
        $Credits = "www.TotusFood.com";
        $titlePrinter = __('Impresora') . ": " . $content['title'];
        $Host    = __('IP') . ": " . $content['host'];
        $Source  = __('Directorio') . ": " . $content['source'];
        $Port    = __('Puerto') . ": " . $content['port'];
        $this->printer->selectCharacterTable(255);
        $this->printer->setTextSize(1, 2);
		$this->printer->setJustification(Printer::JUSTIFY_CENTER);
        $this->printer->text(Helper::validText(ucwords($title)) . "\n");
        $this->printer->setTextSize(1, 1);
        $this->printer->selectPrintMode();
        $this->printer->text(Helper::validText($message) . "\n \n");
		$this->printer->setJustification();
        $this->drawLine();
        $this->printer->text("{$titlePrinter} \n");

        if ($Host != "") {
            $this->printer->text("{$Host} \n");
        }

        if ($Source != "") {
            $this->printer->text("{$Source} \n");
        }

        if ($Port != "") {
            $this->printer->text("{$Port} \n");
        }
		
		$this->printer->setJustification(Printer::JUSTIFY_CENTER);
        $this->printer->text("www.TotusFood.com");
		$this->printer->setJustification();
		$this->printer->text("");
		$this->printer->text("");
        $this->printer->cut();
        $this->printer->close();

            $response = [
                'title' => __('¡Listo!'),
                'status' => 200,
                'message' => __("Prueba de impresión se realizó correctamente") . ".",

            ];

        } catch (\Exception $exception) {
            $response = [
                'title' => __('¡Error!'),
                'status' => 500,
                'message' => __('Error imprimiendo, por favor verifica el estado de impresora, alimentación de energía, puertos de conexión e intente nuevamente') . "."
            ];
        }
        return $response;

    }

    public function open_drawer()
    {
        $this->printer->pulse();
        $this->printer->close();
    }

    function drawLine()
    {
        $new = '';
        for ($i = 1; $i < $this->char_per_line; $i++) {
            $new .= '-';
        }
        return $new . "\n";

    }

    function printLine($str, $size = NULL, $sep = "^", $space = NULL)
    {
        if (!$size) {
            $size = $this->char_per_line;
        }
        $size = $space ? $space : $size;
        $length = strlen($str);
        list($first, $second) = explode("^", $str, 2);
        $line = $first . ($sep == "^" ? $sep : '');
        for ($i = 1; $i < ($size - $length); $i++) {
            $line .= ' ';
        }
        $line .= ($sep != "^" ? $sep : '') . $second;
        $line = str_replace("^", " ", $line);
        return $line;
    }
}
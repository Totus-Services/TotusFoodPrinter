<?php
Route::group(['middleware' => [
    'cors',
],  'prefix' => 'printer',], function () {

    Route::post('update', [
        'as' => 'printer.update',
        'uses' => 'PrintController@updatePredetermined',
    ]);


});
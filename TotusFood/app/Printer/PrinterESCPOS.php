<?php

namespace App\Printer;

use App\Printer\Enum\Printer;
use App\Printer\Libraries\pdf;
use App\Printer\Template\Invoice;
use App\Printer\Template\PrinterTest;
use App\Printer\Template\InvoiceRoot;
use Mike42\Escpos\ImagickEscposImage;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use App\Printer\Template\CommandKitchen;
use voku\Html2Text\Html2Text;


class PrinterESCPOS
{
    /**
     * @var object $connector
     *  Return connection with printer, or null if this is a local connection.
     */
    private $connector;

    /**
     * @var string $typeConnection
     *  Determine if the type connection is local or network.
     */
    private  $typeConnection;

    /**
     * @var string $src
     *  This is a string name printer, if type connection is local, example "POS-58".
     */
    private  $src;

    /**
     * @var string $hostname
     *  The hostname of the target machine, or null if this is a local connection.
     */
    private  $hostname;

    /**
     * @var string $port
     *  This is port if type connection is network.
     */
    private  $port;

    /**
     * @var string $response
     *  This is port if type connection is network.
     */
    private  $response;

    /**
     * @$commandKitchen Object that is instantiated in CommandKitchen and injected as dependencies
     */
    private $commandKitchen;
    private $testPrinter;
    private $invoice;
    private $invoiceRoot;

    function __construct(CommandKitchen $commandKitchen, Invoice $invoice,PrinterTest $testPrinter, InvoiceRoot $invoiceRoot)
    {
        $this->typeConnection;
        $this->commandKitchen   = $commandKitchen;
        $this->invoice          =  $invoice;
        $this->testPrinter      = $testPrinter;
        $this->invoiceRoot      = $invoiceRoot;
    }


    /**
     * This function return the connection, established with the thermal printer.
     * @param $typeConnection
     * @return NetworkPrintConnector|WindowsPrintConnector|null
     */
    public function connection($typeConnection)
    {
        try {
            switch ($typeConnection) {
                case Printer::$local: {
                    $this->connector  = new WindowsPrintConnector($this->src);
                        $this->response = $this->connector;
                    break;
                }
                case Printer::$network: {
                    $this->connector  = new NetworkPrintConnector($this->hostname, $this->port);
                    $this->response = $this->connector;
                    break;
                }
            }

        } catch (\Exception $exception) {
            $this->response = (object) [
                'error' => true,
                'message' => $exception->getMessage()
            ];
        }
        return $this->response;
    }


    /**
     * This function This received data of controller or models, also type connection, source, hostname, port, which is used to build the impression for commands of kitchen.
     * @param $content object This received data of controller or models.
     * @param $typeConnection
     * @param null $src
     * @param null $hostname
     * @param null $port
     */
    public function impression($content, $typeConnection, $src = null, $hostname = null, $port = null , $typePrint)
    {
        try{
            $this->src = $src;
            $this->hostname = $hostname;
            $this->port = $port;
            $this->connector = $this->connection($typeConnection);
            if(isset($this->connector->error)) {
                $error = $this->connector->message;
                $message = (explode(":", $error));
                $response = [
                    'title' => __('¡Error!'),
                    'status' => 500,
                    'message' => __('No se puede conectar a una impresora, por favor verifique el estado, puertos de conexión e intente nuevamente.'),
                ];
            } else {
                if ($typePrint == Printer::$command) {
                    $response = $this->commandKitchen->printer($content, $this->connector);
                } else {
                    $response = $this->invoice->printer($content, $this->connector);
                }
            }
        }catch (\Exception $exception) {
            $response = (object) [
                'error' => true,
                'message' => __('No se puede conectar a una impresora, por favor verifique el estado, puertos de conexión e intente nuevamente') . "."
            ];
        }
        return $response;
    }


    public function TestPrinter($content, $typeConnection, $src = null, $hostname = null, $port = null , $paper)
    {
        try {
            $this -> src = $src;
            $this -> hostname = (string) $hostname;
            $this -> port = (string) $port;
            $this -> connector = $this -> connection((string) $typeConnection);
            if(isset($this->connector->error)) {
                $error = $this->connector->message;
                $message = (explode(":", $error));
                $response = [
                    'title' => __('¡Error!'),
                    'status' => 500,
                    'message' => __('No se puede conectar a una impresora, por favor verifica el estado, puertos de conexión e intente nuevamente.'),
                ];
            } else {
                $response = $this -> testPrinter->printer($content, $this->connector, $paper);
            }

        }catch (\Exception $exception) {
            $response = (object) [
                'error' => true,
                'message' => __('No se puede conectar a una impresora, por favor verifica el estado, puertos de conexión e intente nuevamente') . "."
            ];
        }
        return $response;
    }

    public function TestPrinterConnection($content, $typeConnection, $src = null, $hostname = null, $port = null , $paper)
    {
        try {
            $this -> src = $src;
            $this -> hostname = (string) $hostname;
            $this -> port = (string) $port;
            $this -> connector = $this -> connection((string) $typeConnection);
            if(isset($this->connector->error)) {
                $error = $this->connector->message;
                $message = (explode(":", $error));
                $response = [
                    'title' => __('¡Error!'),
                    'status' => 500,
                    'message' => __('No se puede conectar a una impresora, por favor verifica el estado, puertos de conexión e intente nuevamente.'),
                ];
            } else {
                $response = [
                    'title' => __('¡Listo!'),
                    'status' => 200,
                    'message' => __("Impresora está preparada para realizar impresiones") . ".",

                ];
            }

        }catch (\Exception $exception) {
            $response = (object) [
                'error' => true,
                'message' => __('No se puede conectar a una impresora, por favor verifica el estado, puertos de conexión e intente nuevamente') . "."
            ];
        }
        return $response;
    }

    public function PrinterRoot($content, $typeConnection, $src = null, $hostname = null, $port = null , $paper)
    {
        try {
            $this -> src = $src;
            $this -> hostname = $hostname;
            $this -> port = $port;
            $this -> connector = $this -> connection($typeConnection);
            if(isset($this->connector->error)) {
                $error = $this->connector->message;
                $message = (explode(":", $error));
                $response = [
                    'title' => __('¡Error!'),
                    'status' => 500,
                    'message' => __('No se puede conectar a una impresora, por favor verifica el estado, puertos de conexión e intente nuevamente.'),
                ];
            } else {
                $response = $this->invoiceRoot->printer($content, $this->connector, $paper);
            }

        }catch (\Exception $exception) {
            $response = (object) [
                'error' => true,
                'message' => __('No se puede conectar a una impresora, por favor verifica el estado, puertos de conexión e intente nuevamente') . "."
            ];
        }
        return $response;
    }

}